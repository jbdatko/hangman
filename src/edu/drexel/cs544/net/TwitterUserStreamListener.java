package edu.drexel.cs544.net;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import edu.drexel.cs544.net.util.TweetCache;

import twitter4j.DirectMessage;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;

/**
 * Twitter User Stream Listener using the streaming API for an authenticated
 * twitter account.
 * 
 * @author Josh Datko
 * 
 */
public class TwitterUserStreamListener implements UserStreamListener {

	/**
	 * The twitter api
	 */
	protected Twitter twitter;
	/**
	 * Cache where the tweet are put, this must be set in the constructor
	 */
	protected TweetCache cache;
	/**
	 * This accounts screen name
	 */
	protected String myName;
	/**
	 * This accounts ID number
	 */
	protected long myID;

	/**
	 * Date for debugging
	 */
	protected Date d = new Date();

	/**
	 * Creates a listener using the streaming API
	 * 
	 * @param t
	 *            Instance of the twitter API
	 * @param tc
	 *            Instance of the cache, in which tweets are place
	 */
	public TwitterUserStreamListener(Twitter t, TweetCache tc) {
		if (null == t || null == tc)
			throw new NullPointerException();

		this.twitter = t;
		this.cache = tc;

		try {
			this.myName = t.getScreenName();
			this.myID = t.getId();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Become a hermit and remove all friends
	 */
	public void unFollowAll() {
		try {
			for (long i : this.twitter.getFollowersIDs(-1).getIDs()) {

				// Friend all those who are following
				this.twitter.destroyFriendship(i);

			}
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Follows all followers and unfollows friends who are not also following
	 */
	protected void followAll() {
		try {

			Set<Long> friends = new HashSet<Long>();
			Set<Long> followers = new HashSet<Long>();

			for (long f : this.twitter.getFriendsIDs(-1).getIDs())
				friends.add(f);

			for (long f : this.twitter.getFollowersIDs(-1).getIDs())
				followers.add(f);

			// Friend all new followers
			Set<Long> followersMinusFriends = new HashSet<Long>(followers);
			if (followersMinusFriends.removeAll(friends)
					&& !followersMinusFriends.isEmpty()) {
				// Make new friends :)
				for (long f : followersMinusFriends)
					this.twitter.createFriendship(f);
			}

			// Unfriend all that are not following
			if (friends.removeAll(followers) && !friends.isEmpty())
				for (long u : friends)
					this.twitter.destroyFriendship(u);

		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatus(Status s) {
		if (!s.getUser().getScreenName().equalsIgnoreCase(this.myName)) {
			// Don't put our own tweets in the cache
			this.cache.put(s);
			System.out.println("Received [" + s.getText() + "].");
		}

	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onException(Exception e) {
		System.out.println("Twitter streaming exception: " + e.getMessage()
				+ " " + this.d.toString());

	}

	@Override
	public void onBlock(User arg0, User arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDeletionNotice(long arg0, long arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDirectMessage(DirectMessage arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFavorite(User arg0, User arg1, Status arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFollow(User arg0, User arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFriendList(long[] arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRetweet(User arg0, User arg1, Status arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnblock(User arg0, User arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnfavorite(User arg0, User arg1, Status arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListCreation(User arg0, UserList arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListDeletion(User arg0, UserList arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListMemberAddition(User arg0, User arg1, UserList arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListMemberDeletion(User arg0, User arg1, UserList arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListSubscription(User arg0, User arg1, UserList arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListUnsubscription(User arg0, User arg1, UserList arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserListUpdate(User arg0, UserList arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserProfileUpdate(User arg0) {
		// TODO Auto-generated method stub

	}

}
