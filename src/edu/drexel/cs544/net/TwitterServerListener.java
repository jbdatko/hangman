package edu.drexel.cs544.net;

import edu.drexel.cs544.net.util.TweetCache;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Twitter listener that uses the UserStream API. A TwitterServerListener is for
 * the Twitter Server Socket Factory to detect following events and (maybe) get
 * quicker status updates.
 * 
 * Not all methods have interesting iplementations.
 * 
 * @author Josh Datko
 * 
 */

public class TwitterServerListener extends TwitterUserStreamListener {



	/**
	 * Create a Server Listener 
	 * @param t the twitter api
	 * @param tc the tweet cache to place tweets
	 */
	public TwitterServerListener(Twitter t, TweetCache tc) {
		super(t,tc);

		this.followAll();
	}


	@Override
	public void onFollow(User source, User followedUser) {

		if (source.getScreenName().equalsIgnoreCase(this.myName)) {
			// Ignore our own follow actions
			return;
		}
		try {
			System.out.println("New Follower!!");
			this.twitter.createFriendship(source.getId());
			System.out.println("Now Following: " + source.getScreenName());
		} catch (TwitterException e) {

			// If already following this will cause an exception, but it's not a
			// problem, just continue

			e.printStackTrace();
		}

	}

	

}
