package edu.drexel.cs544.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import edu.drexel.cs544.net.util.TweetCache;
import edu.drexel.cs544.net.util.TwitterTag;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Twitter specific input stream over a logical twitter socket. The remote
 * endpoint screen name is specified during by the socket management.
 * 
 * @author Josh Datko
 * @since 18 May 2012
 * 
 */
public class TwitterInputStream extends InputStream {

	/**
	 * Screen name the remote account.
	 */
	protected String userName;

	/**
	 * Name of the application account, i.e. this account
	 */
	protected String myScreenName;

	/**
	 * Setter for screen name
	 * @param myScreenName the screen name of this account
	 */
	private final void setMyScreenName(String myScreenName) {
		this.myScreenName = myScreenName;
	}

	/**
	 * Instance of the twitter api
	 */
	protected Twitter twitter;
	/**
	 * The maximum ID processed by this inputstream
	 */
	protected long since_id = 1;

	/**
	 * @return the Since ID
	 */
	public final long getSinceID() {
		return since_id;
	}

	/**
	 * Flag to indicate when the input stream is closed
	 */
	private boolean closed = false;

	/**
	 * Cache of retrieved tweets
	 */
	protected TweetCache cache;

	/**
	 * Creates a non-configured InputStream
	 * 
	 * @param t
	 *            Instance of the twitter API
	 * @param c
	 *            Instance of the twitter cache
	 */
	public TwitterInputStream(Twitter t, TweetCache c) {

		if (null == t || null == c)
			throw new NullPointerException("Twitter API can't be null");

		this.twitter = t;
		this.cache = c;

		try {
			this.setMyScreenName(twitter.getScreenName());
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Creates a configured InputSteam to receive tweets from the passed in
	 * screen name
	 * 
	 * @param dst_name
	 *            The remote screen name from which to receive tweets
	 * @param t
	 *            instance of the twitter API
	 */
	public TwitterInputStream(String dst_name, Twitter t, TweetCache c) {

		if (null == t || null == dst_name || null == c)
			throw new NullPointerException("Twitter API can't be null");

		this.cache = c;

		// The twitter API does not use the @ symbol, so strip it off if it is
		// there
		if (dst_name.startsWith("@"))
			this.setRemoteEndpoint(dst_name.substring(1));
		else
			this.setRemoteEndpoint(dst_name);

		this.twitter = t;

		// Update to match the actual screen name
		try {
			this.setRemoteEndpoint(t.showUser(this.getRemoteEndpoint())
					.getScreenName());

			// Get our screen name
			this.setMyScreenName(twitter.getScreenName());
		} catch (TwitterException e) {
			System.out.println(e.toString());
			// Twitter is not available now, so stick with the regular screen
			// name
		}
	}

	/**
	 * Sets the since ID, ensuring it grows up
	 * 
	 * @param id
	 *            the id to set
	 */
	private void setSinceID(long id) {
		if (id > this.since_id)
			this.since_id = id;
	}

	/**
	 * Initializes the input stream by obtaining the most recent mentioned
	 * tweet. All tweets prior to this one will be ignored.
	 * 
	 * @throws IOException
	 *             if can't connect to Twitter or a twitter related error occurs
	 * @throws TwitterException 
	 * 
	 */
	public void init(long id) throws IOException {

		if (1 == id) {

			this.setSinceID(TwitterSocketImpl.getInitialSinceID(twitter));

		} else
			this.setSinceID(id);

	}
	

	@Override
	public int read() throws IOException {
		// Required to override, but not supported
		throw new UnsupportedOperationException(
				"Single byte reads are not supported");
	}

	/**
	 * This method will block until a tweet is received
	 * 
	 * @return The tweet
	 * @throws IOException
	 *             if there is a Twitter connection error
	 */
	public String read_wait() throws IOException {

		if (this.closed)
			throw new IOException("Inputstream is closed");

		boolean msgRec = false;
		String msg = null;

		// Loop until there is a tweet from the dst user. i.e. if there are
		// multiple conversations, we only care about the user at the end of
		// this
		// socket.
		while (!msgRec) {
			Queue<Status> q = this.cache.get_wait(since_id);

			Status s = null;

			// Grab the oldest tweet
			Stack<Status> tweets = new Stack<Status>();

			// Reverse the queue
			while (!q.isEmpty())
				tweets.add(q.poll());

			while (!tweets.isEmpty() && !msgRec) {
				s = tweets.pop();
				
				this.setSinceID(s.getId());

				// Is this message for me?

				if (this.remoteMatch(s)) {

					msg = TwitterTag.removeTag((s.getText()));

					msgRec = true;
					// Check for disconnect
					if (msg.contains(TwitterSocketImpl.DISCONNECT_KEYWORD)) {

						throw new SocketException("Remote end disconnect");
					}
				}
			}

		}

		return msg;

	}
	
	/**
	 * Checks to see if the tweet is intended for this socket
	 * @param s the tweet
	 * @return true if the tweet is part of this conversation
	 */
	private boolean remoteMatch(Status s){
		//This this message from the right user?
		String tweetRemote = s.getUser().getScreenName();
		String setRemote   = this.getRemoteEndpoint();
		boolean fromDst = tweetRemote.contains(setRemote);
		
		//Is this message to me?
		String tag = TwitterTag.getTag(s.getText());
		boolean toMe = false;
		
		if (null != tag)
			toMe = tag.contains(this.myScreenName);
		
		
		return (fromDst && toMe);
	}

	/**
	 * This is a non-blocking read to get the next tweet. If there are multiple
	 * tweets available, this will return the oldest tweet first.
	 * 
	 * @return The tweet which could be null if there are no new tweets
	 * @throws IOException
	 *             if there is a twitter connection error
	 */
	@Deprecated
	public String readNext() throws IOException {
		if (this.closed)
			throw new IOException("Inputstream is closed");

		Queue<Status> q = this.cache.get(since_id);

		if (null == q || q.isEmpty())
			return null;

		Status s = q.poll();

		this.setSinceID(s.getId());

		return TwitterTag.removeTag((s.getText()));

	}

	/**
	 * This is a blocking read, which polls twitter until there is a mention of
	 * the this screen name
	 * 
	 * @param inUse
	 *            The set of already in use screen names
	 * 
	 * @return The screen name of the user mentioned us
	 * @throws IOException
	 *             If there is twitter connection error or if there is an
	 *             interrupt
	 */
	public String waitForMention(Set<String> inUse) throws IOException {
		if (this.closed)
			throw new IOException("Inputstream is closed");

		boolean newConnection = false;

		while (!newConnection) {
			Queue<Status> tweets = this.cache.get_wait(since_id);

			if (null == tweets)
				throw new NullPointerException("Queue should not be empty");
			
			Stack<Status> s = new Stack<Status>();
			
			while (!tweets.isEmpty())
				s.add(tweets.poll());

			while (!s.isEmpty()) {
				Status tweet = s.pop();
				this.setSinceID(tweet.getId());
				
				boolean alreadyConnected = false;
				boolean containsHello    = false;
				
				if (inUse.contains(tweet.getUser().getScreenName()))
					alreadyConnected = true;
				else if (tweet.getText().contains(TwitterSocketImpl.CONNECT_KEYWORD))
					containsHello = true;
				
				if (!alreadyConnected && containsHello) {
					this.setRemoteEndpoint(tweet.getUser().getScreenName());
					s.clear();
					this.since_id--; // Minus one so we can read this msg again
					newConnection = true;
				}

			}

		}

		return this.getRemoteEndpoint();
	}

	@Override
	public void close() {
		// Closes the stream. Once closed it can't be reopened
		this.closed = true;

	}

	/**
	 * Gets the remote endpoint screen name
	 * 
	 * @return the remote name
	 */
	public String getRemoteEndpoint() {
		return this.userName;
	}

	/**
	 * Sets the remote name
	 * 
	 * @param dst
	 */
	private void setRemoteEndpoint(String dst) {
		this.userName = dst;
	}

	/**
	 * Gets the local twitter screen name
	 * 
	 * @return This accounts screen name
	 */
	public String getLocalEndpoint() {
		return this.myScreenName;
	}

}
