package edu.drexel.cs544.net;

import java.net.InetSocketAddress;

/**
 * Extended address for twitter. Twitter "addresses" are screen names.
 * Technically, classes that extend InetSocketAddress should be a traditional
 * TCP socket style (port and address), but to work with the java socket API, I
 * needed to make the twitter address "look like" a socket address.
 * 
 * @author Josh Datko
 * 
 */
@SuppressWarnings("serial")
public class TwitterSocketAddress extends InetSocketAddress {

	/**
	 * The screen name i.e. the twitter address
	 */
	protected String account_name;

	/**
	 * Creates a new
	 * 
	 * @param account
	 *            The twitter screen name
	 */
	public TwitterSocketAddress(String account) {
		super(0);
		this.account_name = account;
	}

	@Override
	public String toString() {

		return this.account_name;
	}

}
