package edu.drexel.cs544.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.HashSet;
import javax.net.ServerSocketFactory;

import edu.drexel.cs544.net.util.TweetCache;
import edu.drexel.cs544.net.util.TweetRetrieverThread;
import edu.drexel.cs544.net.util.TwitterAccountRepository;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * The TwitterServerSocketFactory creates the twitter API instance, configures
 * the account settings, and creates server sockets for the application
 * 
 * @author Josh Datko
 * 
 */
public class TwitterServerSocketFactory extends ServerSocketFactory {

	/**
	 * Instance of the twitter API
	 */
	protected Twitter twitter = null;
	/**
	 * Instance of the Twitter socket implementation, which does all the
	 * detailed socket-level work
	 */
	protected TwitterSocketImpl impl = null;

	/**
	 * Singleton instance of the factory
	 */
	private static TwitterServerSocketFactory factory;

	/**
	 * Tweet cache, where incoming tweets are stored.
	 */
	protected TweetCache cache;

	/**
	 * Instance of the authenticated twitter stream
	 */
	protected TwitterStream stream;
	
	/**
	 * The authentication object for OAuth
	 */
	protected OAuthAuthorization auth;
	
	private TwitterAccountRepository repo = new TwitterAccountRepository();

	/**
	 * Default Constructor
	 * @param account 
	 */
	private TwitterServerSocketFactory(String account) {

		// Hardcoded configuration settings at the moment for the server
		ConfigurationBuilder cb = new ConfigurationBuilder();

		if (!this.repo.config(cb, account))
			throw new NullPointerException("Account not found");

		Configuration conf = cb.build();

		TwitterFactory tf = new TwitterFactory(conf);
		this.twitter = tf.getInstance();

		TwitterSocketImpl.setResources(new HashSet<String>());

		this.cache = new TweetCache();

		// Launch the retriever thread
		new TweetRetrieverThread(this.cache, twitter);

		// Launch the Twitter streaming API Listener
		this.auth = new OAuthAuthorization(conf);
		this.stream = new TwitterStreamFactory().getInstance(auth);
		this.stream.addListener(new TwitterServerListener(this.twitter, this.cache));
		
		// user() method internally creates a thread which manipulates
		// TwitterStream and calls these adequate listener methods continuously.
		this.stream.user();

		this.impl = new TwitterSocketImpl(twitter, this.cache);

	}

	/**
	 * Singleton style get instance method for the factory
	 * 
	 * @return the factory instance
	 */
	public static TwitterServerSocketFactory getInstance() {
		if (null == TwitterServerSocketFactory.factory)
			//factory = new TwitterServerSocketFactory("BotOfHangman");
			factory = new TwitterServerSocketFactory("HangmanBot");

		return TwitterServerSocketFactory.factory;

	}
	
	public static TwitterServerSocketFactory getInstance(String account) {
		if (null == TwitterServerSocketFactory.factory)
			factory = new TwitterServerSocketFactory(account);

		return TwitterServerSocketFactory.factory;
	}

	@Override
	public ServerSocket createServerSocket(int port) throws IOException {

		return this.createServerSocket();

	}

	@Override
	public ServerSocket createServerSocket(int port, int backlog)
			throws IOException {
		return this.createServerSocket();
	}

	@Override
	public ServerSocket createServerSocket(int port, int backlog,
			InetAddress ifAddress) throws IOException {

		return this.createServerSocket();
	}

	@Override
	public ServerSocket createServerSocket() throws IOException {

		// Specify the unique twitter socket implementation
		TwitterServerSocket.setSocketFactory(this.impl);

		// Return a new Twitter server socket
		return new TwitterServerSocket(twitter, this.cache);

	}

}
