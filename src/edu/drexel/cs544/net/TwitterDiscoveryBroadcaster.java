package edu.drexel.cs544.net;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Broadcaster thread to announce the server to the Twitterverse
 * 
 * @author Josh Datko
 * 
 */
public class TwitterDiscoveryBroadcaster implements Runnable {

	/**
	 * Twitter API
	 */
	private Twitter twitter;
	/**
	 * This accounts screen name
	 */
	private String host;
	/**
	 * Delay timer to wait until next broadcast
	 */
	private static final long BROADCAST_DELAY = 1000 * 60 * 59; // 59 minutes
	/**
	 * Instance of the thread, we only want one thread
	 */
	private static Thread daemon = null;

	/**
	 * Create a new Broadcast thread if one is not running
	 * 
	 * @param t
	 *            the twitter API
	 * @param host
	 *            the screen name of this account
	 */
	TwitterDiscoveryBroadcaster(Twitter t, String host) {
		this.twitter = t;
		if (null == host)
			throw new NullPointerException();

		this.host = host;

		// Only want one thread
		if (null == daemon || !daemon.isAlive()) {
			TwitterDiscoveryBroadcaster.daemon = new Thread(this, "Broadcaster");
			TwitterDiscoveryBroadcaster.daemon.setDaemon(true);

			TwitterDiscoveryBroadcaster.daemon.start();
		}
	}

	@Override
	public void run() {

		try {
			if (!this.isAnnounced())
				TwitterServerDiscovery.broadcast(this.twitter);

			while (true) {

				// Sleep for a long time
				Thread.sleep(BROADCAST_DELAY);
				// Ok, time to let people know we are alive
				TwitterServerDiscovery.broadcast(this.twitter);

			}
		} catch (TwitterException e) {

			System.out.println("Broadcaster failed to tweet");
		} catch (InterruptedException e) {

			System.out.println(this.getClass().getName()
					+ " woken from slumber");
		} finally {
			// die
		}

	}

	/**
	 * Checks to see if there was a recent announcement from this server. We
	 * don't want to clog up the HASH_TAG with a bunch of announcements, so if
	 * an announcement is found that is recent, consider the server announced
	 * 
	 * @return true if already announced
	 * @throws TwitterException
	 *             if there is a problem connecting to twitter
	 */
	private boolean isAnnounced() throws TwitterException {

		Query query = new Query(TwitterServerDiscovery.HASH_TAG);

		QueryResult result;

		query.sinceId(1);
		result = twitter.search(query);
		for (Tweet tweet : result.getTweets()) {
			if (tweet.getFromUser().equalsIgnoreCase(this.host))
				// already announced
				return true;
		}

		return false;
	}

}
