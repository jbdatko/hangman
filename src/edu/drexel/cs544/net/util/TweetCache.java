package edu.drexel.cs544.net.util;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;
import twitter4j.ResponseList;
import twitter4j.Status;

/**
 * Repository for tweets that mention the current user. As the name implies
 * 
 * @author Josh Datko
 * 
 */
public class TweetCache {

	/**
	 * Cache in which, the tweets are kept
	 */
	protected ResponseList<Status> cache;

	/**
	 * Adds the collection of tweets to the cache
	 * 
	 * @param newTweets
	 *            New tweets to be cached
	 */
	public synchronized void put(ResponseList<Status> newTweets) {
		if (null != newTweets) {
			if (null == this.cache)
				this.cache = newTweets;
			else {

				ListIterator<Status> li = newTweets.listIterator(newTweets
						.size());

				// Pull from the back of newTweets and add to the front of the
				// cache
				while (li.hasPrevious()) {
					
					this.put(li.previous());
				}

			}

		}

		// Notify all waiters
		notifyAll();

	}

	/**
	 * Put a single tweet into the cache, dups will be silently discarded
	 * 
	 * @param tweet
	 *            the new tweet to enter.
	 */
	public synchronized void put(Status tweet) {
		if (null == tweet)
			throw new NullPointerException("Null tweet");

		if (!this.cache.contains(tweet)) {
			this.cache.add(0, tweet);
			this.notifyAll();
		}
	}

	/**
	 * Returns all cached tweets, will wait until put is called.
	 * 
	 * @return All cached tweets. Could be null if there are no cached tweets.
	 */
	public synchronized Queue<Status> get() {
		Queue<Status> q = null;

		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (null != this.cache) {

			q = new LinkedList<Status>();

			q.addAll(this.cache);

		}

		return q;
	}

	/**
	 * Gets cached tweets newer than max_id
	 * 
	 * @param Most
	 *            recent tweet id
	 * @return queue of tweets newer than since_id. Could be null if there are
	 *         no new tweets.
	 */
	public synchronized Queue<Status> get(long since_id) {

		Queue<Status> q = this.get();
		Queue<Status> result = new LinkedList<Status>();

		if (null != q) {

			while (!q.isEmpty()) {
				Status s = q.poll();
				if (s.getId() > since_id)
					result.add(s);
			}
		}

		return result;
	}

	/**
	 * Blocks until a new tweet is received newer than the max_id
	 * 
	 * @param since_id
	 *            Ignore tweets older than this value
	 * @return New tweets
	 */
	public Queue<Status> get_wait(long since_id) {
		Queue<Status> q = this.get(since_id);

		while (q.isEmpty())
			q = this.get(since_id);

		return q;
	}

}
