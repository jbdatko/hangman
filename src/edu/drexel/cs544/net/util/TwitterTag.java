package edu.drexel.cs544.net.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tag for addressing at the twitter layer
 * 
 * @author Josh Datko
 * 
 */
public class TwitterTag {

	/**
	 * Regular expression that matches just the tag
	 */
	private static Pattern tag = Pattern.compile("<t .*?/t>");
	/**
	 * Regular expression that matches the whole tweet, if it contains the tag
	 */
	private static Pattern tweetWithTag = Pattern.compile("^.*?<t .*?/t>.*$");

	/**
	 * The maximum size of the tag
	 */
	public static final int MAX_TAG_LENGTH = 40;

	/**
	 * Remove the tag from the string
	 * 
	 * @param tweet
	 *            The tweet, with tag
	 * @return The tweet without the tag. If the tag is not present, it will
	 *         return the original string.
	 */
	public static String removeTag(String tweet) {

		if (null == tweet)
			throw new NullPointerException("Null remove addressing parameters");

		Matcher m = tag.matcher(tweet);

		try {
			tweet = m.replaceAll("");
		} catch (Exception e) {

		}

		return tweet;
	}

	/**
	 * Returns the tag from a possible tagged tweet
	 * @param tweet 
	 * @return the tag or null if there was no tag
	 */
	public static String getTag(String tweet) {

		if (null == tweet)
			throw new NullPointerException("Null remove addressing parameters");

		Matcher m = tag.matcher(tweet);

		String result = null;

		try {
			if (m.find())
				result = m.group();
		} catch (Exception e) {

		}

		return result;

	}

	/**
	 * Bulid the tage for the twitter layer
	 * 
	 * @param dst
	 *            the destination twitter account
	 * @param num
	 *            a sequence number
	 * @return the appended string
	 * @throws IOException
	 */
	public static String buildTag(String dst, long num) throws IOException {
		String username = new String(dst);

		if (!username.startsWith("@"))
			username = "@" + username;

		return buildTag(username + " " + num);
	}

	/**
	 * Build the tag around the msg
	 * 
	 * @param msg
	 *            the payload
	 * @return a properly tagged message
	 * @throws IOException
	 */
	public static String buildTag(String msg) throws IOException {
		String tag = "<t " + msg + "/t>";
		if (tag.length() > MAX_TAG_LENGTH)
			throw new IOException("Tag is too big");

		return tag;
	}

	/**
	 * @param tweet
	 * @return true if this tweet has a tag
	 */
	public static boolean hasTag(String tweet) {
		if (null == tweet)
			throw new NullPointerException("null tweet");

		Matcher m = tweetWithTag.matcher(tweet);

		return m.matches();

	}

}
