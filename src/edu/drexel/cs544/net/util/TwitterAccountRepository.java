package edu.drexel.cs544.net.util;

import java.util.HashMap;

import twitter4j.conf.ConfigurationBuilder;

/**
 * Repository that stores account credentials
 * 
 * @author Josh Datko
 * 
 */
public class TwitterAccountRepository {

	private static final boolean TWITTER_DEBUG = false;

	/**
	 * Bundles up the required Twitter keys
	 * 
	 * @author Josh Datko
	 * 
	 */
	protected class OAuthConfig {

		private String consumerKey;
		private String consumerSecret;
		private String accessToken;
		private String tokenSecret;

		/**
		 * Creates a new config bundle
		 * 
		 * @param consumerKey
		 * @param consumerSecret
		 * @param accessToken
		 * @param tokenSecret
		 */
		public OAuthConfig(String consumerKey, String consumerSecret,
				String accessToken, String tokenSecret) {
			if (null == consumerKey || null == consumerSecret
					|| null == accessToken || null == tokenSecret)
				throw new NullPointerException();

			this.consumerKey = consumerKey;
			this.consumerSecret = consumerSecret;
			this.accessToken = accessToken;
			this.tokenSecret = tokenSecret;
		}

		/**
		 * Configures the builder
		 * 
		 * @param cb
		 *            the configuration builder instance
		 */
		public void config(ConfigurationBuilder cb) {
			if (null == cb)
				throw new NullPointerException("Builder is null");

			cb.setDebugEnabled(TWITTER_DEBUG)
					.setOAuthConsumerKey(this.consumerKey)
					.setOAuthConsumerSecret(this.consumerSecret)
					.setOAuthAccessToken(this.accessToken)
					.setOAuthAccessTokenSecret(this.tokenSecret);

		}
	}

	/**
	 * Repository for account information
	 */
	private HashMap<String, OAuthConfig> repo = new HashMap<String, OAuthConfig>();

	/**
	 * Build the repository
	 */
	public TwitterAccountRepository() {
		this.repo.put("gallowsdude", new OAuthConfig("EKGwqO1pCuIBfCIupFkA",
				"KdypS7z6bdke1ADpR61SjfphcReOJtEnRC1I7hHaZM",
				"582465234-tTjh8j8ZEHihCr3hTk2PmXGfSpzzfMGrxLtX8qT2",
				"hYW3zoK9uzJS2K4iXfnB46QBm7NlHQ6hC1D1DyDjKVw"));

		this.repo.put("galgenmann", new OAuthConfig("gXsC5LVfwPAK8qt0r2uRVA",
				"C2emZ1OHrQ5c4yQLdOXLsFexDfmB9DTP5MoeObQOpoE",
				"601355381-33SI5c7rLNdfj1oZT9dzUJYPXyDNFIJTsXZyScCQ",
				"s04VLXMrN0ufn3eCirIJgCikd2WWpKirSeoLv0U3Wo"));

		this.repo.put("hangmanbot", new OAuthConfig("49n19P7joAv4daUziJYXBg",
				"bbHusMDvhBrlSYzuxywO1uHva4Ihr3mHfgbUFc",
				"564541668-UW8fJvCOsWmx7LLhSJ8m9GlUN6weQx67Rg3hpZVJ",
				"w2reKpIpSgn8eZTE6PdO4A4XTUlYADWd0UChagqibg"));

		this.repo.put("botofhangman", new OAuthConfig("rIzQvy1na1EIpt5QNhFN5A",
				"yleWFsD4mGXW0cpWGumddfsvgl5Fky9RIueWHKxbVw",
				"603036230-hyfpgUXelyPz8gxe4U6F5K7rxr12aqKd6dZkaHs7",
				"IlOMZl9I6Of5QROwjSBf1O5EW1jwrSQ9AeT2gIZ2WQ"));
	}

	/**
	 * Configures the account
	 * 
	 * @param cb
	 *            The configuration builder
	 * @param account
	 *            the name of the account
	 * @return true if the configuration is successful, otherwise false
	 */
	public boolean config(ConfigurationBuilder cb, String account) {

		if (null == account || null == cb)
			throw new NullPointerException();

		account = account.toLowerCase();

		OAuthConfig settings = repo.get(account);
		if (null == settings)
			return false;
		else
			settings.config(cb);

		return true;

	}
	
	/**
	 * @param account
	 * @return true if the account is in the repository
	 */
	public boolean contains(String account){
		if (null == account)
			throw new NullPointerException();
		
		account = account.toLowerCase();
		
		return this.repo.containsKey(account);
	}

}
