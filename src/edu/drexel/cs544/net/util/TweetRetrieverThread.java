package edu.drexel.cs544.net.util;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * 
 * @author Josh Datko
 * 
 */
public class TweetRetrieverThread implements Runnable {

	/**
	 * The synchronized tweet cache
	 */
	protected TweetCache cache;
	/**
	 * Twitter API
	 */
	protected Twitter twitter;
	/**
	 * High water mark that represents most recent tweet processed
	 */
	long since_id = 1;
	/**
	 * Twitter enforces 350 GET calls an hour from an authenticated client,
	 * which comes out to just above one get every 10 seconds
	 */
	public static final long WAIT_TIME = 12500;

	/**
	 * Creates a new thread to get tweets
	 * @param c The tweet cache, in which this class will deposit tweets
	 * @param t Twitter API
	 */
	public TweetRetrieverThread(TweetCache c, Twitter t) {
		if (null == c || null == t)
			throw new NullPointerException();

		this.cache = c;
		this.twitter = t;
		

		Thread tr = new Thread(this, "TweetRetriever");
		
		//This is an utility thread
		tr.setDaemon(true);
		
		tr.start();
		
		
	}

	@Override
	public void run() {

		while (true) {
			try {
				//Get mentions only newer than the since ID
				ResponseList<Status> mentions = this.twitter
						.getMentions(new Paging(this.since_id));

				if (!mentions.isEmpty()) {
					//Remember the latest tweet id
					this.since_id = mentions.get(0).getId();
				}

				this.cache.put(mentions);

			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					Thread.sleep(TweetRetrieverThread.WAIT_TIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

}
