package edu.drexel.cs544.net;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import edu.drexel.cs544.net.util.TwitterTag;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.StatusUpdate;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Twitter Discovery Service thread. Will discover Hangman servers in the
 * Twitterverse
 * 
 * @author Josh Datko
 * 
 */
public class TwitterServerDiscovery implements Runnable {

	/**
	 * The hash tag, to which twitter servers will broadcast
	 */
	public static final String HASH_TAG = "#hangman";
	/**
	 * Since ID, the latest tweet read by this server
	 */
	private static long SINCE_ID = 1;
	/**
	 * the keyword, which must be in the tweet
	 */
	public static final String DISCOVER_KEYWORD = "^discover^";
	/**
	 * The search API has rate limits, like the REST API, so we don't wont to
	 * poll constantly. Every 30 seconds should be fine.
	 */
	private static final int SLEEP_TIMER = 30000;
	/**
	 * Set of discovered servers
	 */
	private static Set<String> servers = new HashSet<String>();
	/**
	 * A daemon thread to do the polling
	 */
	private static Thread daemon = null;

	/**
	 * Create the discovery instance, and lanuch a new daemon thread if it is
	 * not already running
	 */
	public TwitterServerDiscovery() {
		if (null == daemon) {
			daemon = new Thread(this, "ServerDiscoveryDaemon");
			daemon.setDaemon(true);
			daemon.start();
		}

		if (!daemon.isAlive()) {
			daemon = new Thread(this, "ServerDiscoveryDaemon");
			daemon.setDaemon(true);
			daemon.start();

		}

	}

	/**
	 * Utility method to get the broadcast string
	 * 
	 * @return the properly formed broadcast string
	 */
	private static String getBroadcastMessage() {
		Date d = new Date();
		return HASH_TAG + DISCOVER_KEYWORD + " " + d.toString();
	}

	/**
	 * Try to discover Hangman Servers, will loop forever until one is found
	 * 
	 * @return the name of a newly discovered server
	 */
	private void discover() {

		Twitter twitter = new TwitterFactory().getInstance();

		Query query = new Query(HASH_TAG);

		QueryResult result;

		// Will loop until a new server is found
		boolean newServerFound = false;
		
		while (!newServerFound) {
			try {
				query.sinceId(SINCE_ID);
				result = twitter.search(query);
				for (Tweet tweet : result.getTweets()) {

					setSinceID(tweet.getId());
					if (TwitterTag.hasTag(tweet.getText())
							&& tweet.getText().contains(DISCOVER_KEYWORD)) {

						if (servers.add(tweet.getFromUser()))
							newServerFound = true;
						// else, it was already discovered

					}
				}
			} catch (TwitterException e) {

				System.out.println("Discovery service is grumbling");
			}

			try {
				Thread.sleep(SLEEP_TIMER);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * Sets the since ID, ensuring that it only grows bigger
	 * 
	 * @param id
	 *            the new id to set
	 */
	private static void setSinceID(long id) {
		if (id > SINCE_ID)
			SINCE_ID = id;

	}

	/**
	 * 
	 * @return the list of servers discovered, this could be empty
	 */
	public synchronized List<String> getServers() {
		return new LinkedList<String>(servers);
	}

	@Override
	public void run() {

		// Loop until new server is found
		this.discover();
		

	}
	
	@Override
	public String toString(){
		Queue<String> q = new LinkedList<String>(this.getServers());
		
		String result = "Discovered servers: ";
		while (!q.isEmpty())
			result = result + q.poll() + " ";
		
		return result;
		
		
		
	}

	/**
	 * Announce to the Twitterverse the server is up and running
	 * 
	 * @param twitter
	 *            the twitter api
	 * @throws TwitterException
	 *             if there is a problem connecting to twitter
	 */
	public static void broadcast(Twitter twitter) throws TwitterException {

		// Add the twitter layer tag

		String msg = null;
		
		try {
			msg = getBroadcastMessage() + TwitterTag.buildTag("ANNOUNCE");
		} catch (IOException e) {
			//Should never happen with a fixed size parameter...
			throw new TwitterException("Failed creating announce tag");
		}

		StatusUpdate latestUpdate = new StatusUpdate(msg);

		twitter.updateStatus(latestUpdate);
		System.out.println("BROADCASTING Online status");

	}

}
