package edu.drexel.cs544.net;

import java.io.IOException;
import java.net.ProtocolException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;

import edu.drexel.cs544.net.util.TweetCache;

import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Extended ServerServer for Twitter sockets
 * 
 * @author Josh Datko
 * 
 */
public class TwitterServerSocket extends ServerSocket {

	/**
	 * Instance of the twitter API
	 */
	protected Twitter twitter;
	
	protected HashMap<String, Socket> socketMap;
	
	protected TweetCache cache;

	/**
	 * Creates an instance of a ServerSocket, which mainly exists to accept new
	 * incoming connections. The constructor will attempt to access the twitter
	 * network immediately.
	 * 
	 * @param t
	 *            An instance of the twitter API
	 * @throws IOException
	 *             if there is a Twitter connection error.
	 */
	public TwitterServerSocket(Twitter t, TweetCache c) throws IOException {
		super();

		if (null == t || null == c)
			throw new NullPointerException("Null Constructor paramaters");

		this.twitter = t;
		this.cache = c;
		
	

		try {
			// The act of getting the screen name reaches the Twitter network

			this.bind(new TwitterSocketAddress(t.getScreenName()));

		} catch (IllegalStateException e) {
			throw new ProtocolException(e.getMessage());
		} catch (TwitterException e) {
			throw new ProtocolException("Twitter: " + e.getMessage());
		}
	}

	@Override
	public Socket accept() throws IOException {

		// This method is very closely modeled off of the Java 1.6 accept
		// method.

		if (isClosed())
			throw new SocketException("Socket is closed");
		if (!isBound())
			throw new SocketException("Socket is not yet bound");

		// This is the key difference, we need to specify a TwitterSocket here
		TwitterSocket s = new TwitterSocket(twitter, this.cache);
		
		

		implAccept(s);

		return s;
	}

}
