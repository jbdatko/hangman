package edu.drexel.cs544.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;

import javax.net.SocketFactory;

import edu.drexel.cs544.net.util.TweetCache;
import edu.drexel.cs544.net.util.TweetRetrieverThread;
import edu.drexel.cs544.net.util.TwitterAccountRepository;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Extended SocketFactory for creating TwitterSockets. Uses a singleton style
 * design to ensure that only one instance of the factory is created.
 * 
 * @author Josh Datko
 * 
 */
public class TwitterSocketFactory extends SocketFactory {

	/**
	 * Instance of the factory.
	 */
	private static TwitterSocketFactory factory;
	/**
	 * Instance of the Twitter API
	 */
	private Twitter twitter = null;

	/**
	 * Tweet cache, where tweets are stored
	 */
	private TweetCache cache;
	/**
	 * OAuth configuration instance, needed to setup streaming API
	 */
	private OAuthAuthorization auth;
	/**
	 * Instance of the authenticated streaming API object
	 */
	private TwitterStream stream;

	private TwitterAccountRepository repo = new TwitterAccountRepository();

	/**
	 * Private constructor to support singleton. Creates and configures the
	 * twitter API
	 * 
	 * @param screenName
	 *            The screen name of the client to use
	 */

	private TwitterSocketFactory(String screenName) {
		// Hard coded twitter client for the time being

		ConfigurationBuilder cb = new ConfigurationBuilder();
		
		if (!this.repo.config(cb, screenName))
			throw new NullPointerException("Account not Found");

		Configuration conf = cb.build();

		TwitterFactory tf = new TwitterFactory(conf);
		this.twitter = tf.getInstance();

		TwitterSocketImpl.setResources(new HashSet<String>());

		this.cache = new TweetCache();

		// Launch the Twitter streaming API Listener
		this.auth = new OAuthAuthorization(conf);
		this.stream = new TwitterStreamFactory().getInstance(auth);
		this.stream.addListener(new TwitterUserStreamListener(this.twitter,
				this.cache));

		// user() method internally creates a thread which manipulates
		// TwitterStream and calls these adequate listener methods continuously.
		this.stream.user();

		// Launch the retriever thread
		new TweetRetrieverThread(this.cache, twitter);
	};


	/**
	 * Creates, if required the factory.
	 * 
	 * @return An instance of the socket factory
	 */
	public static TwitterSocketFactory getInstance() {
		String screenName = "gallowsDude";

		return TwitterSocketFactory.getInstance(screenName);
	}

	/**
	 * Creates, if required the factory with the given screen name
	 * 
	 * @param screenName
	 *            the screen name to use
	 * @return An instance of the socket factory
	 */
	public static TwitterSocketFactory getInstance(String screenName) {
		if (null == screenName || screenName.length() < 2)
			throw new NullPointerException();

		if (screenName.startsWith("@"))
			screenName = screenName.substring(1);
		if (null == TwitterSocketFactory.factory)
			TwitterSocketFactory.factory = new TwitterSocketFactory(screenName);

		return TwitterSocketFactory.factory;
	}

	@Override
	public Socket createSocket(String host, int port) throws IOException,
			UnknownHostException {

		// Creates and connects a twitter socket
		TwitterSocket soc = new TwitterSocket(this.twitter, this.cache);

		soc.connect(new TwitterSocketAddress(host));

		return soc;

	}

	@Override
	public Socket createSocket(InetAddress host, int port) throws IOException {

		throw new UnsupportedOperationException(
				"Only the string parameter version of this method is supported");

	}

	@Override
	public Socket createSocket(String host, int port, InetAddress localAddress,
			int localPort) throws IOException, UnknownHostException {

		throw new UnsupportedOperationException(
				"Only the string parameter version of this method is supported");
	}

	@Override
	public Socket createSocket(InetAddress address, int port,
			InetAddress localAddress, int localPort) throws IOException {
		throw new UnsupportedOperationException(
				"Only the string parameter version of this method is supported");
	}

}
