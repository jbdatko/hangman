package edu.drexel.cs544.net.test;


import twitter4j.DirectMessage;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;
import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class TestFollowers {
	
	static Twitter t;
	
	static UserStreamListener listener = new UserStreamListener() {
		


		@Override
		public void onDeletionNotice(StatusDeletionNotice arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onScrubGeo(long arg0, long arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStatus(Status arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTrackLimitationNotice(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onException(Exception arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onBlock(User arg0, User arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onDeletionNotice(long arg0, long arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onDirectMessage(DirectMessage arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFavorite(User arg0, User arg1, Status arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFollow(User source, User followedUser) {
			try {
				System.out.println("New Follower!!");
				
				t.createFriendship(source.getId());
				System.out.println("Now Following: " + source.getName());
			} catch (TwitterException e) {
				
				//If already following this will cause an exception, but it's not a problem, just continue
			}
			
		}

		@Override
		public void onFriendList(long[] arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRetweet(User arg0, User arg1, Status arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUnblock(User arg0, User arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUnfavorite(User arg0, User arg1, Status arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListCreation(User arg0, UserList arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListDeletion(User arg0, UserList arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListMemberAddition(User arg0, User arg1, UserList arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListMemberDeletion(User arg0, User arg1, UserList arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListSubscription(User arg0, User arg1, UserList arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListUnsubscription(User arg0, User arg1, UserList arg2) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserListUpdate(User arg0, UserList arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onUserProfileUpdate(User arg0) {
			// TODO Auto-generated method stub
			
		}


	};

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Hardcoded configuration settings at the moment for the server
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
				.setOAuthConsumerKey("49n19P7joAv4daUziJYXBg")
				.setOAuthConsumerSecret(
						"bbHusMDvhBrlSYzuxywO1uHva4Ihr3mHfgbUFc")
				.setOAuthAccessToken(
						"564541668-UW8fJvCOsWmx7LLhSJ8m9GlUN6weQx67Rg3hpZVJ")
				.setOAuthAccessTokenSecret(
						"w2reKpIpSgn8eZTE6PdO4A4XTUlYADWd0UChagqibg");
				//.setPassword("(nuc\"zhG;/Si(38Ma0Wr")
				//.setUser("HangmanBot");
		
		Configuration conf = cb.build();
		
		
		TwitterFactory tf = new TwitterFactory(conf);
		t = tf.getInstance();
		
		TwitterStream twitterStream = null;
		
		OAuthAuthorization auth = new OAuthAuthorization(conf);
		
		System.out.println(auth);
		
		twitterStream = new TwitterStreamFactory().getInstance(auth);

        twitterStream.addListener(listener);
        // user() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
        
        twitterStream.user();
		
		
		
		
	}

}
