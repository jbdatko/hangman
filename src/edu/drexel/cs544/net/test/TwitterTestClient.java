package edu.drexel.cs544.net.test;

import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import javax.net.SocketFactory;
import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;
import edu.drexel.cs544.net.TwitterSocketFactory;
import edu.drexel.cs544.util.LetterGuessMessage;
import edu.drexel.cs544.util.MessageException;

public class TwitterTestClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String serverName = args[0];
		
		if (null == serverName) {
			System.out.println("Must type in the account name as the first argument, i.e. gallowDude");
			System.exit(0);
		}
		
		SocketFactory fac = TwitterSocketFactory.getInstance(serverName);
		
		Double version = (double)1;
		int seq = 1;
		
		LetterGuessMessage m = null;
		try {
			m = new LetterGuessMessage(version, seq);
			m.setDataField(generateRandomWords(1, 1).toUpperCase());
		} catch (MessageException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Queue<String> run = new LinkedList<String>();
		run.add("HM/1.0/000/CONN//");
		run.add("HM/1.0/002/GREQ/NEW//");
		run.add("HM/1.0/004/LGUE/A//");
		run.add("HM/1.0/006/SGUE/HELLO//");
		run.add("HM/1.0/008/SGUE/HELLO//");
		run.add("HM/1.0/010/GREQ/NEW//");
		run.add("HM/1.0/012/ENDG//");
		run.add("HM/1.0/014/DISC//");
		

		try {
			Socket soc = fac.createSocket("@HangmanBot", 0);

			TwitterOutputStream out = (TwitterOutputStream) soc
					.getOutputStream();
			
			TwitterInputStream in = (TwitterInputStream) soc.getInputStream();
			
			while (!run.isEmpty()) {
				
				
				out.write(run.poll());
	
				
				System.out.println(in.read_wait());
			}
			
			
			
			soc.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.exit(0);
		}

	}

	public static String generateRandomWords(int min, int max) {
		// code obtained from
		// http://stackoverflow.com/questions/4951997/generating-random-words-in-java
		// for testing purposes only
		String randomWord = null;

		Random random = new Random();

		char[] word = new char[random.nextInt(max) + min]; // words of length
															// min
															// through max

		for (int j = 0; j < word.length; j++) {
			word[j] = (char) ('a' + random.nextInt(26));
		}
		randomWord = new String(word);

		return randomWord;
	}

}
