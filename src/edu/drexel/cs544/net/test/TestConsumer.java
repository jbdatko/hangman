package edu.drexel.cs544.net.test;

import java.util.LinkedList;
import java.util.Queue;

import edu.drexel.cs544.net.util.TweetCache;

import twitter4j.Status;

public class TestConsumer implements Runnable {

	Queue<Status> q;
	TweetCache cache;
	long max_id = 1;
	int counter = 0;
	
	public TestConsumer(TweetCache c){
		this.cache = c;
		this.q = new LinkedList<Status>();
		new Thread(this, "TestConsumer").start();
		
	}
	@Override
	public void run() {
		
		while (true){
			Queue<Status> sq = this.cache.get_wait(max_id);
			
			if (null != sq ){
				System.out.println("Current max_ID: " + max_id + " Counter: " + counter);
				counter++;
				while (!sq.isEmpty()){
					Status s = sq.poll();
					System.out.println(s.getText());
					if (s.getId() >= max_id)
						max_id = s.getId();
					
				}
				System.out.println("Current max_ID: " + max_id);
				
			}

			
			
			
		}

	}

}
