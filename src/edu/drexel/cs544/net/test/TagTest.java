package edu.drexel.cs544.net.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import edu.drexel.cs544.net.util.TwitterTag;

public class TagTest {

	@Test
	public void test() {
		String base = "sdafjasdfjsadlf";
		String tagger = "<t @bobyoursmonkey 1234/t>";
		String one = base + tagger;
		String two = base;

		
		assertEquals(null, two, TwitterTag.removeTag(two));
		
		assertEquals(null, base, TwitterTag.removeTag(one));

		try {
			assertEquals(null,"<t @username 1234/t>", TwitterTag.buildTag("@username", 1234));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(TwitterTag.hasTag(one));
		assertFalse(TwitterTag.hasTag(base));
		
		String tag = TwitterTag.getTag(one);
		
		assertTrue(tag.equals(tagger));
		
		
		
	}

}
