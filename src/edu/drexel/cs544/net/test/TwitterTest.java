package edu.drexel.cs544.net.test;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.net.SocketFactory;
import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;
import edu.drexel.cs544.net.TwitterServerSocketFactory;
import edu.drexel.cs544.net.TwitterSocketFactory;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;


/**
 * Sandbox test code for the twitter layer
 * @author jdatko
 *
 */
public class TwitterTest {

	/**
	 * @param args
	 */
	
	public static void tweet(Twitter twitter, String msg){
		
		StatusUpdate latestUpdate = new StatusUpdate(msg);
		Status status;
		try {
			status = twitter.updateStatus(latestUpdate);
			System.out.println("Successfully updated the status to [" + status.getText() + "].");
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	public static void main(String[] args) {
		

		TwitterServerSocketFactory serverFac = TwitterServerSocketFactory.getInstance();
		SocketFactory clientFac = TwitterSocketFactory.getInstance();
		
		try {
			
			
			ServerSocket serverSocket = serverFac.createServerSocket();
			
			Socket soc = serverSocket.accept();
			TwitterInputStream in = (TwitterInputStream) soc.getInputStream();
			
			System.out.println(in.readNext());
			System.out.println(in.readNext());
		} catch (Exception e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		
		//Twitter twitter = new TwitterFactory().getInstance();
		//TwitterOutputStream writer = new TwitterOutputStream("@GallowsDude", twitter);
		//TwitterInputStream reader = new TwitterInputStream("@GallowsDude", twitter);
		
		Socket s = null;
		try {
			s = clientFac.createSocket("@HangmanBot", 0);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		TwitterOutputStream out = null;
		try {
			out = (TwitterOutputStream) s.getOutputStream();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		try {

			out.write("This is a test4");
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		Query query = new Query("#hangman");
//	    QueryResult result;
//		try {
//			result = twitter.search(query);
//			for (Tweet tweet : result.getTweets()) {
//		        System.out.println(tweet.getFromUser() + ":" + tweet.getText());
//		    }
//		} catch (TwitterException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	    

	}

}
