package edu.drexel.cs544.net.test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;
import edu.drexel.cs544.net.TwitterServerSocketFactory;

public class TwitterTestServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TwitterServerSocketFactory fac = TwitterServerSocketFactory
				.getInstance();

		Socket soc = null;
		
		ServerSocket server = null;
		try {
			server = fac.createServerSocket();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (true) {
			
			try {
				

				soc = server.accept();

				TwitterInputStream in = (TwitterInputStream) soc
						.getInputStream();
				TwitterOutputStream out = (TwitterOutputStream) soc
						.getOutputStream();

				while (true) {
					String msgRec = in.read_wait();

					if (msgRec.contains("quit")) {
						soc.close();
						break;
					}

					msgRec = "I agree: " + msgRec;

					out.write(msgRec);

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					soc.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
