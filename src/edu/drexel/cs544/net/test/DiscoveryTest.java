package edu.drexel.cs544.net.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import edu.drexel.cs544.net.TwitterServerDiscovery;

public class DiscoveryTest {

	@Test
	public void test() {
		
		TwitterServerDiscovery d = new TwitterServerDiscovery();
		TwitterServerDiscovery d1 = new TwitterServerDiscovery();
		
		List<String> servers = d.getServers();
		assertTrue(servers.isEmpty());
		assertTrue(d1.getServers().isEmpty());
		

	}

}
