package edu.drexel.cs544.net;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.util.Date;
import java.util.Random;

import edu.drexel.cs544.net.util.TwitterTag;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Extended OutputStream to write data to a twitter socket
 * 
 * @author Josh Datko
 * 
 */
public class TwitterOutputStream extends OutputStream {

	/**
	 * The destination twitter screen name
	 */
	protected String dst_name;
	/**
	 * An instance of the twitter API
	 */
	protected Twitter twitter;
	/**
	 * Flag to indicate if the stream is closed
	 */
	private boolean closed = false;

	/**
	 * Sequence number for outgoing messages
	 */
	private long seqNum;
	
	/**
	 * The maximum size of the payload, not including the tag
	 */
	public static final int MAX_PAYLOAD_LENGTH = 100;
	
	/**
	 * Delay after a write, to help with rate limits
	 */
	private static final long SEND_DELAY = 10000; // 10 sec
	

	/**
	 * Creates a configured output stream
	 * 
	 * @param name
	 *            The destination screen name
	 * @param t
	 *            Instance of the twitter API
	 */
	public TwitterOutputStream(String name, Twitter t) {
		if (null == name || null == t)
			throw new NullPointerException("Null Paramaters into constructor");

		// The twitter API does not use the @ symbol, so strip it off if it is
		// there
		if (name.startsWith("@"))
			this.setRemoteEndpoint(name.substring(1));
		else
			this.setRemoteEndpoint(name);

		this.twitter = t;

		Date d = new Date();
		Random r = new Random(d.getTime());

		this.seqNum = r.nextInt(5000);
	}

	@Override
	public void write(int arg0) throws IOException {
		// Required to override, but not supported
		throw new UnsupportedOperationException(
				"Single byte writes are not supported");

	}

	/**
	 * Tweets the status and adds appropiate information for the Twitter
	 * Adaption Layer
	 * 
	 * @param msg
	 *            the status msg to tweet
	 * @throws IOException
	 *             if there is a Twitter Error
	 */
	public void write(String msg) throws IOException {
		
		if (null == msg)
			throw new NullPointerException("Message to write is null");

		if (this.closed)
			throw new IOException("Stream is closed");
		
		if (msg.length() > MAX_PAYLOAD_LENGTH)
			throw new IOException("Payload size too big");

		// Add the twitter layer tag

		msg = msg + TwitterTag.buildTag(this.getRemoteEndpoint(), this.seqNum);


		StatusUpdate latestUpdate = new StatusUpdate(msg);

		try {
			
			Status status = twitter.updateStatus(latestUpdate);
			System.out.println("Sent [" + status.getText() + "].");
			this.seqNum++;
			
			Thread.sleep(SEND_DELAY);
		} catch (TwitterException e) {

			throw new ProtocolException("Twitter: " + e.getErrorMessage());
		} catch (InterruptedException e) {
			
			//Sleep interrupted, not great, but ok
			
		}

	}

	@Override
	public void close() {
		// Shuts down the stream. A closed outputstream can't be reopened.
		this.closed = true;

	}

	/**
	 * Gets the remote endpoint screen name
	 * 
	 * @return the remote name
	 */
	public String getRemoteEndpoint() {
		return this.dst_name;
	}

	/**
	 * Sets the remote name
	 * 
	 * @param dst
	 */
	private void setRemoteEndpoint(String dst) {
		this.dst_name = dst;
	}

}
