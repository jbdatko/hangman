package edu.drexel.cs544.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ProtocolException;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import java.net.SocketImplFactory;
import java.util.Date;
import java.util.Set;

import edu.drexel.cs544.net.util.TweetCache;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * The Guts Of The Twitter Socket Implementation. A Twitter Socket Is defined As
 * (Hst_screenname, Dst_screenname). Data Is Written To The Users Screen Name,
 * But The At Mention Of The Destination Screen Name Is Added In This Twitter
 * Adaption Layer. The receive (input data stream) is based on twitter at
 * mentions. So, the dst endpoint, if he tweets with the src screen name, that
 * is seen as an incoming, or received tweet.
 * 
 * @Author Josh Datko
 * 
 */
public class TwitterSocketImpl extends SocketImpl implements SocketImplFactory {

	/**
	 * Instance of the twitter api
	 */
	protected Twitter twitter;
	/**
	 * Host endpoint twitter screen name
	 */
	protected String host;
	/**
	 * Destination endpoint screen name
	 */
	protected String dst;
	/**
	 * Output stream, will write to the dst
	 */
	protected TwitterOutputStream out;
	/**
	 * Input stream, will receive from the dst
	 */
	protected TwitterInputStream in;

	/**
	 * Set of in use connections
	 */
	protected static Set<String> resources;

	/**
	 * Tweet cache, where tweets are stored
	 */
	protected TweetCache cache;

	public static final String DISCONNECT_KEYWORD = "^bye^";

	public static final String CONNECT_KEYWORD = "^hello^";

	private static long SINCE_ID = 1;

	/**
	 * Creates a new implementation instance, typically for the calling socket
	 * 
	 * @param t
	 *            the twitter api instance
	 * @param cache
	 * @throws TwitterException
	 * @throws IllegalStateException
	 */
	public TwitterSocketImpl(Twitter t, TweetCache c) {
		this.twitter = t;
		this.cache = c;
		try {
			this.host = twitter.getScreenName();
		} catch (Exception e) {
			throw new NullPointerException("Screen name unavailable");
		}
	}

	@Override
	public Object getOption(int optionID) throws SocketException {
		// A no-op for twitter based sockets
		return null;
	}

	@Override
	public void setOption(int arg0, Object arg1) throws SocketException {
		// A no-op for twitter based sockets

	}

	/**
	 * Grabs the SINCE_ID only if SINCE_ID is initialized to 1
	 * 
	 * @param c
	 *            the tweet cache from which the tweet are obtained
	 * @return the starting Since ID
	 */
	public static long getInitialSinceID(Twitter t) throws IOException {

		try {
			long sinceID = 1;

			ResponseList<Status> mentions = t.getHomeTimeline(new Paging(
					sinceID));
			if (mentions.size() > 0) {
				Status s = mentions.get(0);
				sinceID = s.getId();
			}

			return sinceID;

		} catch (TwitterException e) {
			throw new IOException("Twitter: " + e.getMessage());
		}

	}

	@Override
	protected void accept(SocketImpl s) throws IOException {
		if (null == TwitterSocketImpl.resources)
			throw new NullPointerException("Resource table null");

		SINCE_ID = getInitialSinceID(this.twitter);

		if (s.getClass().isInstance(this)) {
			boolean connectionFound = false;

			TwitterSocketImpl t = (TwitterSocketImpl) s;
			t.in = new TwitterInputStream(this.twitter, this.cache);
			// Init the input stream with the latest since ID
			t.in.init(SINCE_ID);

			// Launch a new broadcaster if one is not already running
			new TwitterDiscoveryBroadcaster(this.twitter, this.host);

			while (!connectionFound) {

				t.dst = t.in.waitForMention(TwitterSocketImpl.resources);

				t.out = new TwitterOutputStream(t.dst, this.twitter);

				Date d = new Date();

				String msg = t.in.read_wait();

				// Mark this as the latest message
				SINCE_ID = t.in.getSinceID();

				if (msg.contains(TwitterSocketImpl.CONNECT_KEYWORD)
						&& !TwitterSocketImpl.resources.contains(t.dst)) {

					TwitterSocketImpl.resources.add(t.dst);

					// new incoming connection
					t.out.write(TwitterSocketImpl.CONNECT_KEYWORD
							+ d.toString());

					connectionFound = true;

					System.out.println("Accepted connection from: " + t.dst);

				} else if (msg.contains(TwitterSocketImpl.CONNECT_KEYWORD)
						&& TwitterSocketImpl.resources.contains(t.dst)) {
					// Send back bye
					t.out.write(TwitterSocketImpl.DISCONNECT_KEYWORD
							+ d.toString());

				} else if (!msg.contains(TwitterSocketImpl.CONNECT_KEYWORD)
						&& !TwitterSocketImpl.resources.contains(t.dst)) {
					// Send back bye
					t.out.write(TwitterSocketImpl.DISCONNECT_KEYWORD
							+ d.toString());
				} else {
					// doesn't contain hello, but its in the resource table.
					// This
					// indicates an improperly shutdown socket.
					// This should have been caught elsewhere
					throw new SocketException("Incorrect internal state");
				}
			}
		} else {
			throw new SocketException("Unsupported Socket Implementation");
		}

	}

	@Override
	protected int available() throws IOException {
		// A no-op for twitter based sockets
		return 0;
	}

	@Override
	protected void bind(InetAddress arg0, int arg1) throws IOException {

		// This is essentially a no-op, but it is required to be overridden. If
		// there is a valid screen name for the host, it is considered "bound."

	}

	@Override
	protected void close() throws IOException {
		if (null == this.in || null == this.out)
			throw new NullPointerException("Streams should not be null");

		Date d = new Date();

		// Free the resource
		TwitterSocketImpl.resources.remove(this.dst);

		this.out.write(TwitterSocketImpl.DISCONNECT_KEYWORD + " "
				+ d.toString());

		// Shutdown the streams, which prevents any more data from being sent.
		this.in.close();
		this.out.close();
		

	}

	@Override
	protected void connect(String host, int port) throws IOException {
		throw new UnsupportedOperationException(
				"This style connect method not supported");

	}

	@Override
	protected void connect(InetAddress address, int port) throws IOException {
		throw new UnsupportedOperationException(
				"This style connect method not supported");

	}

	@Override
	protected void connect(SocketAddress address, int timeout)
			throws IOException {
		try {
			// Set the host and destination parameters

			this.dst = ((TwitterSocketAddress) address).toString();

			// Get the dst timeline, which validates the destination user name
			twitter.getUserTimeline(this.dst);

			// Follow the user
			try {
				if (!this.twitter.existsFriendship(this.host, this.dst))
					twitter.createFriendship(this.dst, false);
			} catch (TwitterException e) {
				// Already following
				e.printStackTrace();
			}

			// setup the input stream
			this.in = new TwitterInputStream(this.dst, this.twitter, this.cache);
			// Force init to get its own SINCE_ID
			this.in.init(1);

			// setup the output stream
			this.out = new TwitterOutputStream(this.dst, this.twitter);

			Date d = new Date();

			this.out.write(TwitterSocketImpl.CONNECT_KEYWORD + " "
					+ d.toString());

			if (!this.in.read_wait()
					.contains(TwitterSocketImpl.CONNECT_KEYWORD))
				throw new ProtocolException("Connection Failed");

		} catch (IllegalStateException e) {
			throw new ProtocolException(e.getMessage());
		} catch (TwitterException e) {
			throw new ProtocolException("Twitter: " + e.getMessage());
		}

	}

	@Override
	protected void create(boolean arg0) throws IOException {
		// A no-op for twitter based sockets

	}

	@Override
	protected InputStream getInputStream() throws IOException {

		if (null == this.in) {
			if (null == this.dst)
				throw new NullPointerException("Destination name not set");
			else
				this.in = new TwitterInputStream(this.dst, this.twitter,
						this.cache);
		}
		return this.in;
	}

	@Override
	protected OutputStream getOutputStream() throws IOException {

		// Create the output stream and return
		if (null == this.out)
			this.out = new TwitterOutputStream(this.dst, this.twitter);

		return this.out;

	}

	@Override
	protected void listen(int arg0) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void sendUrgentData(int arg0) throws IOException {
		// A no-op for twitter based sockets

	}

	@Override
	public SocketImpl createSocketImpl() {
		// Returns this instance.
		return this;
	}

	public static void setResources(Set<String> availableResources) {
		if (null != TwitterSocketImpl.resources)
			throw new NullPointerException("Resource Table already set");
		TwitterSocketImpl.resources = availableResources;
	}

}
