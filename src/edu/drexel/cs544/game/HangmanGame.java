package edu.drexel.cs544.game;

import java.net.SocketException;

import edu.drexel.cs544.server.game.GameState;
import edu.drexel.cs544.server.game.Guess;

/**
 * Interface for a Hangman Game
 * 
 *
 */
public interface HangmanGame {
	
	/**
	 * Starts or continues the game
	 * @param gameServer the Game server to connect to
	 * @return True if a game has been started
	 */
	public boolean startGame(String gameServer) throws SocketException;
	
	/**
	 * Get the status of the puzzle
	 * @return the String representation of the puzzle
	 */
	public String getPuzzleStatus();
	
	/**
	 * Send a guess to the game
	 * @param theGuess The player's Guess
	 */
	public void processGuess(Guess theGuess) throws SocketException;
	
	/**
	 * Gets the number of used guess
	 * @return the number of used guess
	 */
	public int usedGuesses();
	
	/**
	 * Returns the total number of guesses allowed
	 * @return Returns the total number of guesses allowed
	 */
	public int guesses();
	
	/**
	 * Get the current game state
	 * @return Get the current game state
	 */
	public GameState getState();
	
	/**
	 * Gets the solution
	 * @return the hangman solution
	 */
	public String correctPhrase();
	
	/**
	 * End the game and perform any necessary cleanup
	 */
	public void endGame() throws SocketException;

}
