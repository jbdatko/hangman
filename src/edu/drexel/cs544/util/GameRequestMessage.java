package edu.drexel.cs544.util;

/**
 * Definition for Game Request Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class GameRequestMessage extends HangmanMessage {

	public final String NEW = "NEW";
	public final String RESUME = "RES";
	
	public GameRequestMessage() {
		super(MessageType.GAME_REQUEST);

	}

	public GameRequestMessage(Double version)
			throws MessageException {
		super(MessageType.GAME_REQUEST, version);

	}

	public GameRequestMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.GAME_REQUEST, version, sequence);

	}
	
	public GameRequestMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.GAME_REQUEST, version, sequence, dataField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!dataField.equals(NEW) && !dataField.equals(RESUME))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
}
