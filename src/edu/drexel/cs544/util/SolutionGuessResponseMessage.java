package edu.drexel.cs544.util;

/**
 * Definition for Solution Guess Response Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class SolutionGuessResponseMessage extends HangmanMessage {

	public SolutionGuessResponseMessage() {
		super(MessageType.SOLUTION_GUESS_RESPONSE);

	}

	public SolutionGuessResponseMessage(Double version)
			throws MessageException {
		super(MessageType.SOLUTION_GUESS_RESPONSE, version);

	}

	public SolutionGuessResponseMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS_RESPONSE, version, sequence);

	}
	
	public SolutionGuessResponseMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS_RESPONSE, version, sequence, dataField);

	}
	
	public SolutionGuessResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS_RESPONSE, version, sequence, dataField, 
				solutionStatusField);

	}
	
	public SolutionGuessResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField);

	}
	
	public SolutionGuessResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField, String gameOverField) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField, gameOverField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!dataField.equals(POSITIVE) && !dataField.equals(NEGATIVE))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
}

