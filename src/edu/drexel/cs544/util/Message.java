package edu.drexel.cs544.util;

/**
 * Interface for setting and getting message data.
 * 
 * @author Brian Wilhelm
 * 
 */
public interface Message {
	
	// method signatures
	
	/**
	 * @return The type of the message
	 * @throws MessageException
	 *             If the message type is not populated
	 */
	MessageType getMessageType() throws MessageException;
	
	/**
	 * Sets the version of the protocol
	 * 
	 * @param version
	 *            The version of the protocol
	 * @throws MessageException
	 *             If the version is invalid
	 */
	void setVersion(Double version) throws MessageException;
	
	/**
	 * @return The version of the message
	 * @throws MessageException
	 *             If the version is not populated
	 */
	String getVersion() throws MessageException;
	
	/**
	 * Sets the sequence of the message
	 * 
	 * @param sequence
	 *            The sequence of the message
	 * @throws MessageException
	 *             If the sequence is invalid
	 */
	void setSequence(Integer sequence) throws MessageException;
	
	/**
	 * @return The sequence of the message
	 * @throws MessageException
	 *             If the sequence is not populated
	 */
	String getSequence() throws MessageException;
	
	/**
	 * Sets the data field of the message
	 * 
	 * @param dataField
	 *            The data field of the message
	 * @throws MessageException
	 *             If the data field is invalid
	 */
	void setDataField(String dataField) throws MessageException;
	
	/**
	 * @return The data field of the message
	 * @throws MessageException
	 *             If the data field is not used or not populated
	 */
	String getDataField() throws MessageException ;
	
	/**
	 * Sets the solution status field of the message
	 * 
	 * @param solutionStatusField
	 *            The solution status field of the message
	 * @throws MessageException
	 *             If the solution status field is invalid
	 */
	void setSolutionStatusField(String solutionStatusField) throws MessageException;
	
	/**
	 * @return The solution status field of the message
	 * @throws MessageException
	 *             If the solution status field is not used or not populated
	 */
	String getSolutionStatusField() throws MessageException ;
	
	/**
	 * Sets the hangman status field of the message
	 * 
	 * @param hangmanStatusField
	 *            The hangman status field of the message
	 * @throws MessageException
	 *             If the hangman status field is invalid
	 */
	void setHangmanStatusField(String hangmanStatusField) throws MessageException;
	
	/**
	 * @return The hangman status field of the message
	 * @throws MessageException
	 *             If the hangman status field is not used or not populated
	 */
	String getHangmanStatusField() throws MessageException;
	
	/**
	 * Sets the game over field of the message
	 * 
	 * @param gameOverField
	 *            The game over field of the message
	 * @throws MessageException
	 *             If the game over field is invalid
	 */
	void setGameOverField(String gameOverField) throws MessageException;
	
	/**
	 * @return The game over field of the message
	 * @throws MessageException
	 *             If the game over field is not used or not populated
	 */
	String getGameOverField() throws MessageException;
	
	/**
	 * @return Whether the game over field has been set
	 * @throws MessageException
	 *             If the game over field is not used
	 */
	boolean isGameOverFieldSet() throws MessageException;
}
