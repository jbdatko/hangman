package edu.drexel.cs544.util;

/**
 * Definition for Game Request Response Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class GameRequestResponseMessage extends HangmanMessage {

	public GameRequestResponseMessage() {
		super(MessageType.GAME_REQUEST_RESPONSE);

	}

	public GameRequestResponseMessage(Double version)
			throws MessageException {
		super(MessageType.GAME_REQUEST_RESPONSE, version);

	}

	public GameRequestResponseMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.GAME_REQUEST_RESPONSE, version, sequence);

	}
	
	public GameRequestResponseMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.GAME_REQUEST_RESPONSE, version, sequence, dataField);

	}
	
	public GameRequestResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField) 
			throws MessageException {
		super(MessageType.GAME_REQUEST_RESPONSE, version, sequence, dataField, 
				solutionStatusField);

	}
	
	public GameRequestResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField) 
			throws MessageException {
		super(MessageType.GAME_REQUEST_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!dataField.equals(POSITIVE) && !dataField.equals(NEGATIVE))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
	
	@Override
	public String getSolutionStatusField() 
			throws MessageException {
		
		if (!this.dataField.equals(NEGATIVE) &&
			(this.solutionStatusField == null || this.solutionStatusField == ""))
			throw new MessageException("Solution status field not populated!");
		
		if (this.dataField.equals(NEGATIVE))
			return "";
		else
			return solutionStatusField;
	}

	@Override
	public String getHangmanStatusField() 
			throws MessageException {
		
		if (!this.dataField.equals(NEGATIVE) &&
			(this.hangmanStatusField == null || this.hangmanStatusField == ""))
			throw new MessageException("Hangman status field not populated!");
		
		if (this.dataField.equals(NEGATIVE))
			return "";
		else
			return this.hangmanStatusField;
	}
}
