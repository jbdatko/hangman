package edu.drexel.cs544.util;

/**
 * Definition for Connect Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class ConnectMessage extends HangmanMessage {

	public ConnectMessage() {
		super(MessageType.CONNECT);

	}

	public ConnectMessage(Double version)
			throws MessageException {
		super(MessageType.CONNECT, version);
	}

	public ConnectMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.CONNECT, version, sequence);

	}
}
