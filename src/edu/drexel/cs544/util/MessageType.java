package edu.drexel.cs544.util;

import java.lang.String;

/**
 * Enum for message types.
 * 
 * @author Brian Wilhelm
 * 
 */
public enum MessageType {
	CONNECT                 ("CONN", false, false, false, false),
	GAME_REQUEST            ("GREQ", true,  false, false, false),
	LETTER_GUESS            ("LGUE", true,  false, false, false),
	SOLUTION_GUESS          ("SGUE", true,  false, false, false),
	END_GAME                ("ENDG", false, false, false, false),
	DISCONNECT              ("DISC", false, false, false, false),
	CONNECT_RESPONSE        ("CRES", true,  false, false, false),
	GAME_REQUEST_RESPONSE   ("GRES", true,  true,  true,  false),
	LETTER_GUESS_RESPONSE   ("LRES", true,  true,  true,  true ),
	SOLUTION_GUESS_RESPONSE ("SRES", true,  true,  true,  true ),
	END_GAME_RESPONSE       ("ERES", true,  true,  true,  false),
	DISCONNECT_RESPONSE     ("DRES", false, false, false, false),
	ERROR_MESSAGE           ("ERRO", true,  false, false, false);
	
	private final String  keyword;
	private final boolean usesDataField;
	private final boolean usesSolutionStatusField;
	private final boolean usesHangmanStatusField;
	private final boolean usesGameOverField;
	
	MessageType (String keyword, boolean usesDataField, boolean usesSolutionStatusField, 
			boolean usesHangmanStatusField, boolean usesGameOverField) {
		this.keyword = keyword;
		this.usesDataField = usesDataField;
		this.usesSolutionStatusField = usesSolutionStatusField;
		this.usesHangmanStatusField = usesHangmanStatusField;
		this.usesGameOverField = usesGameOverField;
	}
	
	public String getKeyword() { return keyword; }
	
	public boolean usesDataField() { return usesDataField; }
	
	public boolean usesSolutionStatusField() { return usesSolutionStatusField; }
	
	public boolean usesHangmanStatusField() { return usesHangmanStatusField; }
	
	public boolean usesGameOverField() { return usesGameOverField; }
}
