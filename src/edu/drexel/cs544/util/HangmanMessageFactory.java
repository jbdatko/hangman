package edu.drexel.cs544.util;

/**
 * Utility for encoding and decoding messages.
 * 
 * @author Brian Wilhelm
 * 
 */
public class HangmanMessageFactory {
	
	private static final String PROTOCOL_IDENTIFIER = "HM";
	private static final String FIELD_DELIMITER     = "/";
	private static final String MESSAGE_DELIMITER   = "//";
	
	private static final int MIN_NUM_FIELDS = 4;
	private static final int MAX_NUM_FIELDS = 8;
	private static final int PROTOCOL_IDENTIFIER_INDEX = 0;
	private static final int VERSION_INDEX = 1;
	private static final int SEQUENCE_INDEX = 2;
	private static final int KEYWORD_INDEX = 3;
	private static final int DATA_INDEX = 4;
	private static final int SOLUTION_STATUS_INDEX = 5;
	private static final int HANGMAN_STATUS_INDEX = 6;
	private static final int GAME_OVER_INDEX = 7;
	
	/**
	 * Encodes a Message object into a String object
	 * 
	 * @param message
	 *            The message to be encoded
	 * @throws MessageException
	 *             Re-throws exceptions to method caller
	 */
	public static String encode(Message message) 
			throws MessageException {
		
		String encoded_message = null;
		
		try {
			//Protocol Identifier
			encoded_message = addProtocolIdentifier(encoded_message);
			//Version
			encoded_message = addField(encoded_message, message.getVersion());
			//Sequence
			encoded_message = addField(encoded_message, message.getSequence());
			//Keyword
			encoded_message = addField(encoded_message, message.getMessageType().getKeyword());
			//Data
			if (message.getMessageType().usesDataField()) {
				encoded_message = addField(encoded_message, message.getDataField());
				//Solution Status
				if (message.getMessageType().usesSolutionStatusField()) {
					encoded_message = addField(encoded_message, message.getSolutionStatusField());
					//Hangman Status
					if (message.getMessageType().usesHangmanStatusField()) {
						encoded_message = addField(encoded_message, message.getHangmanStatusField());
						//Game Over
						if (message.getMessageType().usesGameOverField() && message.isGameOverFieldSet()) {
							encoded_message = addField(encoded_message, message.getGameOverField());
						}
					}
				}
			}
			encoded_message = addMessageDelimiter(encoded_message);
		}
		catch (MessageException exp) {
			throw exp;
		}
		
		return encoded_message;
	}
	
	private static String addField(String message, String field) 
			throws MessageException {
		
		if (message.endsWith(FIELD_DELIMITER) || message.endsWith(MESSAGE_DELIMITER))
			throw new MessageException("Malformed Message");

		if (field == null || field == "")
			return message;
		else
			return message.concat(FIELD_DELIMITER).concat(field);
	}	
	
	private static String addProtocolIdentifier(String message) 
			throws MessageException {
		
		if (message != null)
			throw new MessageException("Malformed Message");

		return PROTOCOL_IDENTIFIER;
	}
	
	private static String addMessageDelimiter(String message) 
			throws MessageException {
		
		if (message.endsWith(FIELD_DELIMITER) || message.endsWith(MESSAGE_DELIMITER))
			throw new MessageException("Malformed Message");
			
		return message.concat(MESSAGE_DELIMITER);
	}
	
	public static Message decode(String message) 
			throws MessageException {
		
		Message decoded_message = null;
		
		try {
			String[] fields = extractFieldsAndCheckMessageStructure(message);
			decoded_message = createMessage(fields[KEYWORD_INDEX]);
			setMessageFields(decoded_message, fields);
		}
		catch (MessageException exp) {
			throw exp;
		}
		
		return decoded_message;
	}
	
	private static String[] extractFieldsAndCheckMessageStructure(String message)
			throws MessageException {
		
		String[] fields = message.split(FIELD_DELIMITER);
		
		if(fields.length < MIN_NUM_FIELDS || fields.length > MAX_NUM_FIELDS)
			throw new MessageException("Malformed Message");
		
		if(!fields[PROTOCOL_IDENTIFIER_INDEX].equals(PROTOCOL_IDENTIFIER))
			throw new MessageException("Malformed Message");
		
		if(!message.endsWith(MESSAGE_DELIMITER))
			throw new MessageException("Malformed Message");
		
		for(int i = 0; i < fields.length; ++i ) {
			if(fields[i] == "")
				throw new MessageException("Malformed Message");
		}
		
		return fields;
	}
	
	private static Message createMessage(String keyword)
			throws MessageException {
		
		Message message = null;
		
		if(keyword.equals(MessageType.CONNECT.getKeyword()))
			message = new ConnectMessage();
		else if(keyword.equals(MessageType.GAME_REQUEST.getKeyword()))
			message = new GameRequestMessage();
		else if(keyword.equals(MessageType.LETTER_GUESS.getKeyword()))
			message = new LetterGuessMessage();
		else if(keyword.equals(MessageType.SOLUTION_GUESS.getKeyword()))
			message = new SolutionGuessMessage();
		else if(keyword.equals(MessageType.END_GAME.getKeyword()))
			message = new EndGameMessage();
		else if(keyword.equals(MessageType.DISCONNECT.getKeyword()))
			message = new DisconnectMessage();
		else if(keyword.equals(MessageType.CONNECT_RESPONSE.getKeyword()))
			message = new ConnectResponseMessage();
		else if(keyword.equals(MessageType.GAME_REQUEST_RESPONSE.getKeyword()))
			message = new GameRequestResponseMessage();
		else if(keyword.equals(MessageType.LETTER_GUESS_RESPONSE.getKeyword()))
			message = new LetterGuessResponseMessage();
		else if(keyword.equals(MessageType.SOLUTION_GUESS_RESPONSE.getKeyword()))
			message = new SolutionGuessResponseMessage();
		else if(keyword.equals(MessageType.END_GAME_RESPONSE.getKeyword()))
			message = new EndGameResponseMessage();
		else if(keyword.equals(MessageType.DISCONNECT_RESPONSE.getKeyword()))
			message = new DisconnectResponseMessage();
		else if(keyword.equals(MessageType.ERROR_MESSAGE.getKeyword()))
			message = new ErrorMessage();
		else
			throw new MessageException("Unknown Keyword");
		
		return message;
	}
	
	private static void setMessageFields(Message message, String[] fields)
			throws MessageException {
		
		try {
			//Version
			message.setVersion(Double.parseDouble(fields[VERSION_INDEX]));
			
			//Sequence
			message.setSequence(Integer.parseInt(fields[SEQUENCE_INDEX]));
		
			//Data
			if(message.getMessageType().usesDataField() && fields.length > DATA_INDEX) {
				message.setDataField(fields[DATA_INDEX]);
			}
			
			//Solution Status
			if(message.getMessageType().usesSolutionStatusField() && fields.length > SOLUTION_STATUS_INDEX) {
				message.setSolutionStatusField(fields[SOLUTION_STATUS_INDEX]);
			}
			
			//Hangman Status
			if(message.getMessageType().usesHangmanStatusField() && fields.length > HANGMAN_STATUS_INDEX) {
				message.setHangmanStatusField(fields[HANGMAN_STATUS_INDEX]);
			}
			
			//Game Over
			if(message.getMessageType().usesGameOverField() && fields.length > GAME_OVER_INDEX) {
					message.setGameOverField(fields[GAME_OVER_INDEX]);
			}
		}
		catch (MessageException exp) {
			throw exp;
		}
		catch (NumberFormatException exp) {
			throw new MessageException("Malformed Message");
		}
	}
}
