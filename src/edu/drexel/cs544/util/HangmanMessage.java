package edu.drexel.cs544.util;

/**
 * Super class for messages with ability to set and get message data.
 * 
 * @author Brian Wilhelm
 * 
 */
public class HangmanMessage implements Message {
	
	public final String POSITIVE = "POS";
	public final String NEGATIVE = "NEG";
	public final String WIN      = "WIN";
	public final String LOSE     = "LOSE";
	public final char   HYPHEN   = '-';
	
	protected final int HANGMAN_STATUS_INCORRECT_GUESSES_INDEX = 0;
	protected final int HANGMAN_STATUS_HYPHEN_INDEX = 1;
	protected final int HANGMAN_STATUS_MAX_INCORRECT_GUESSES_INDEX = 2;
	protected final int HANGMAN_STATUS_LENGTH = 3;

	protected MessageType messageType = null;
	protected Double  version = null;
	protected Integer sequence = null;
	protected String  dataField = null;
	protected String  solutionStatusField = null;
	protected String  hangmanStatusField = null;
	protected String  gameOverField = null;
	protected boolean gameOverFieldSet = false;
	
	private final String VERSION_FORMAT_STRING  = "%.1f";
	private final String SEQUENCE_FORMAT_STRING = "%03d";
	
	public HangmanMessage(MessageType messageType) {
		this.messageType = messageType;
	}
	
	public HangmanMessage(MessageType messageType, Double version) 
			throws MessageException {
		
		try {
			this.messageType = messageType;
			this.setVersion(version);
		}
		catch (MessageException exp) {
			throw exp;
		}
	}
	
	public HangmanMessage(MessageType messageType, Double version, 
			Integer sequence) 
			throws MessageException {
		
		try {
			this.messageType = messageType;
			this.setVersion(version);
			this.setSequence(sequence);
		}
		catch (MessageException exp) {
			throw exp;
		}
	}
	
	public HangmanMessage(MessageType messageType, Double version, 
			Integer sequence, String dataField) 
			throws MessageException {
		
		try {
			this.messageType = messageType;
			this.setVersion(version);
			this.setSequence(sequence);
			this.setDataField(dataField);
		}
		catch (MessageException exp) {
			throw exp;
		}
	}
	
	public HangmanMessage(MessageType messageType, Double version, 
			Integer sequence, String dataField, String solutionStatusField) 
			throws MessageException {
		
		try {
			this.messageType = messageType;
			this.setVersion(version);
			this.setSequence(sequence);
			this.setDataField(dataField);
			this.setSolutionStatusField(solutionStatusField);
		}
		catch (MessageException exp) {
			throw exp;
		}
	}
	
	public HangmanMessage(MessageType messageType, Double version, 
			Integer sequence, String dataField, String solutionStatusField, 
			String hangmanStatusField) 
			throws MessageException {
		
		try {
			this.messageType = messageType;
			this.setVersion(version);
			this.setSequence(sequence);
			this.setDataField(dataField);
			this.setSolutionStatusField(solutionStatusField);
			this.setHangmanStatusField(hangmanStatusField);
		}
		catch (MessageException exp) {
			throw exp;
		}
	}
	
	public HangmanMessage(MessageType messageType, Double version, 
			Integer sequence, String dataField, String solutionStatusField, 
			String hangmanStatusField, String gameOverField) 
			throws MessageException {
		
		try {
			this.messageType = messageType;
			this.setVersion(version);
			this.setSequence(sequence);
			this.setDataField(dataField);
			this.setSolutionStatusField(solutionStatusField);
			this.setHangmanStatusField(hangmanStatusField);
			this.setGameOverField(gameOverField);
		}
		catch (MessageException exp) {
			throw exp;
		}
	}
	
	@Override
	public MessageType getMessageType() 
			throws MessageException {
		
		if (this.messageType == null)
			throw new MessageException("Message type not populated!");
		
		return messageType;
	}

	@Override
	public void setVersion(Double version) 
			throws MessageException {
		
		if (version.doubleValue() < 0.0)
			throw new MessageException("Version not valid!");
		
		this.version = version;
	}

	@Override
	public String getVersion() 
			throws MessageException {
		
		if (this.version == null)
			throw new MessageException("Version not populated!");
		
		return String.format(VERSION_FORMAT_STRING, this.version);
	}

	@Override
	public void setSequence(Integer sequence)
			throws MessageException {
		
		if (sequence.intValue() < 0)
			throw new MessageException("Sequence not valid!");
		
		this.sequence = sequence;
	}

	@Override
	public String getSequence() 
			throws MessageException {
		
		if (this.sequence == null)
			throw new MessageException("Sequence not populated!");
		
		return String.format(SEQUENCE_FORMAT_STRING, this.sequence);
	}

	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!this.messageType.usesDataField())
			throw new MessageException("Data field not used for this message type!");
		
		this.dataField = dataField;
	}

	@Override
	public String getDataField() 
			throws MessageException {
		
		if (!this.messageType.usesDataField())
			throw new MessageException("Data field not used for this message type!");
		if (this.dataField == null || this.dataField == "")
			throw new MessageException("Data field not populated!");
			
		return this.dataField;
	}

	@Override
	public void setSolutionStatusField(String solutionStatusField)
			throws MessageException {
		
		if (!this.messageType.usesSolutionStatusField())
			throw new MessageException("Solution status field not used for this message type!");
		for(int i = 0; i < solutionStatusField.length(); ++i) {
			if (solutionStatusField.charAt(i) != HYPHEN && 
			    !Character.isUpperCase(solutionStatusField.charAt(i)))
				throw new MessageException("Solution status field invalid!");
		}
		
		this.solutionStatusField = solutionStatusField;
	}

	@Override
	public String getSolutionStatusField() 
			throws MessageException {
		
		if (!this.messageType.usesSolutionStatusField())
			throw new MessageException("Solution status field not used for this message type!");
		if (this.solutionStatusField == null || this.solutionStatusField == "")
			throw new MessageException("Solution status field not populated!");
		
		return this.solutionStatusField;
	}

	@Override
	public void setHangmanStatusField(String hangmanStatusField)
			throws MessageException {
		
		if (!this.messageType.usesHangmanStatusField())
			throw new MessageException("Hangman status field not used for this message type!");
		if(hangmanStatusField.length() != HANGMAN_STATUS_LENGTH)
			throw new MessageException("Hangman status field invalid!");
		for(int i = 0; i < HANGMAN_STATUS_LENGTH; ++i) {
			if (!Character.isDigit(hangmanStatusField.charAt(i)) &&
				(hangmanStatusField.charAt(i) != HYPHEN || i != HANGMAN_STATUS_HYPHEN_INDEX))
				throw new MessageException("Hangman status field invalid!");
		}
		
		this.hangmanStatusField = hangmanStatusField;
	}

	@Override
	public String getHangmanStatusField() 
			throws MessageException {
		
		if (!this.messageType.usesHangmanStatusField())
			throw new MessageException("Hangman status field not used for this message type!");
		if (this.hangmanStatusField == null || this.hangmanStatusField == "")
			throw new MessageException("Hangman status field not populated!");
		
		return this.hangmanStatusField;
	}

	@Override
	public void setGameOverField(String gameOverField) 
			throws MessageException {
		
		if (!messageType.usesGameOverField())
			throw new MessageException("Game over field not used for this message type!");		
		if (!gameOverField.equals(WIN) && !gameOverField.equals(LOSE))
			throw new MessageException("Game over field invalid!");
		
		this.gameOverField = gameOverField;
		this.gameOverFieldSet = true;
	}

	@Override
	public String getGameOverField() 
			throws MessageException {
		
		if (!this.messageType.usesGameOverField())
			throw new MessageException("Game over field not used for this message type!");
		if (this.gameOverField == null || this.gameOverField == "")
			throw new MessageException("Game over field not populated!");
		
		return this.gameOverField;
	}
	
	@Override
	public boolean isGameOverFieldSet() 
			throws MessageException {
		
		if (!this.messageType.usesGameOverField())
			throw new MessageException("Game over field not used for this message type!");
		
		return this.gameOverFieldSet;
	}
}
