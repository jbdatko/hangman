package edu.drexel.cs544.util;

/**
 * Custom exception for message related exceptions.
 * 
 * @author Brian Wilhelm
 * 
 */
@SuppressWarnings("serial")
public class MessageException extends Exception {

	private String message = null;
	
	public MessageException() {
		super();
	}

	public MessageException(String message) {
		super(message);
		this.message = message;
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
