package edu.drexel.cs544.util;

/**
 * Definition for End Game Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class EndGameMessage extends HangmanMessage {

	public EndGameMessage() {
		super(MessageType.END_GAME);

	}

	public EndGameMessage(Double version)
			throws MessageException {
		super(MessageType.END_GAME, version);

	}

	public EndGameMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.END_GAME, version, sequence);

	}
}
