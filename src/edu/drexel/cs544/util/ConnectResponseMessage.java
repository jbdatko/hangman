package edu.drexel.cs544.util;

/**
 * Definition for Connect Response Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class ConnectResponseMessage extends HangmanMessage {

	public ConnectResponseMessage() {
		super(MessageType.CONNECT_RESPONSE);

	}

	public ConnectResponseMessage(Double version)
			throws MessageException {
		super(MessageType.CONNECT_RESPONSE, version);

	}

	public ConnectResponseMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.CONNECT_RESPONSE, version, sequence);

	}
	
	public ConnectResponseMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.CONNECT_RESPONSE, version, sequence, dataField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!dataField.equals(POSITIVE) && !dataField.equals(NEGATIVE))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
}
