package edu.drexel.cs544.util;

/**
 * Definition for Disconnect Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class DisconnectMessage extends HangmanMessage {

	public DisconnectMessage() {
		super(MessageType.DISCONNECT);

	}

	public DisconnectMessage(Double version)
			throws MessageException {
		super(MessageType.DISCONNECT, version);

	}

	public DisconnectMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.DISCONNECT, version, sequence);

	}
}
