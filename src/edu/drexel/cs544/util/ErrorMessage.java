package edu.drexel.cs544.util;

/**
 * Definition for Error Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class ErrorMessage extends HangmanMessage {
	
	private final int ERROR_CODE_LENGTH = 3;

	public ErrorMessage() {
		super(MessageType.ERROR_MESSAGE);

	}

	public ErrorMessage(Double version)
			throws MessageException {
		super(MessageType.ERROR_MESSAGE, version);

	}

	public ErrorMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.ERROR_MESSAGE, version, sequence);

	}
	
	public ErrorMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.ERROR_MESSAGE, version, sequence, dataField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		for(int i = 0; i < ERROR_CODE_LENGTH; ++i) {
			if(!Character.isDigit(dataField.charAt(i)))
				throw new MessageException("Data field invalid!");
		}
		
		this.dataField = dataField;
	}
}

