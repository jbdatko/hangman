package edu.drexel.cs544.util;

/**
 * Definition for Dsiconnect Response Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class DisconnectResponseMessage extends HangmanMessage {

	public DisconnectResponseMessage() {
		super(MessageType.DISCONNECT_RESPONSE);

	}

	public DisconnectResponseMessage(Double version)
			throws MessageException {
		super(MessageType.DISCONNECT_RESPONSE, version);

	}

	public DisconnectResponseMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.DISCONNECT_RESPONSE, version, sequence);

	}
}
