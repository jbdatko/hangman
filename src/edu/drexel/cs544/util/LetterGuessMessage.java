package edu.drexel.cs544.util;

/**
 * Definition for Letter Guess Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class LetterGuessMessage extends HangmanMessage {
	
	public LetterGuessMessage() {
		super(MessageType.LETTER_GUESS);

	}

	public LetterGuessMessage(Double version)
			throws MessageException {
		super(MessageType.LETTER_GUESS, version);

	}

	public LetterGuessMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.LETTER_GUESS, version, sequence);

	}
	
	public LetterGuessMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.LETTER_GUESS, version, sequence, dataField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (dataField.length() != 1 || !Character.isUpperCase(dataField.charAt(0)))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
}
