package edu.drexel.cs544.util;

/**
 * Definition for Solution Guess Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class SolutionGuessMessage extends HangmanMessage {
	
	public SolutionGuessMessage() {
		super(MessageType.SOLUTION_GUESS);

	}

	public SolutionGuessMessage(Double version)
			throws MessageException {
		super(MessageType.SOLUTION_GUESS, version);

	}

	public SolutionGuessMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS, version, sequence);

	}
	
	public SolutionGuessMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.SOLUTION_GUESS, version, sequence, dataField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		for(int i = 0; i < dataField.length(); ++i) {
			if (!Character.isUpperCase(dataField.charAt(i)))
				throw new MessageException("Data field invalid!");
		}
		
		this.dataField = dataField;
	}
}
