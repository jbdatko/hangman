package edu.drexel.cs544.util;

/**
 * Definition for End Game Response Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class EndGameResponseMessage extends HangmanMessage {

	public EndGameResponseMessage()
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE);

	}

	public EndGameResponseMessage(Double version)
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE, version);

	}

	public EndGameResponseMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE, version, sequence);

	}
	
	public EndGameResponseMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE, version, sequence, dataField);

	}
	
	public EndGameResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField) 
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE, version, sequence, dataField, 
				solutionStatusField);

	}
	
	public EndGameResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField) 
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField);
		
	}
	
	public EndGameResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField, String gameOverField) 
			throws MessageException {
		super(MessageType.END_GAME_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField, gameOverField);
		
	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!dataField.equals(POSITIVE))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
	
	@Override
	public void setSolutionStatusField(String solutionStatusField)
			throws MessageException {
		
		for(int i = 0; i < solutionStatusField.length(); ++i) {
			if (!Character.isUpperCase(solutionStatusField.charAt(i)))
				throw new MessageException("Solution status field invalid!");
		}
		
		this.solutionStatusField = solutionStatusField;
	}
	
	@Override
	public void setHangmanStatusField(String hangmanStatusField)
			throws MessageException {
		
		if(hangmanStatusField.length() != HANGMAN_STATUS_LENGTH)
			throw new MessageException("Hangman status field invalid!");
		for(int i = 0; i < HANGMAN_STATUS_LENGTH; ++i) {
			if (!Character.isDigit(hangmanStatusField.charAt(i)) &&
				(hangmanStatusField.charAt(i) != HYPHEN || i != HANGMAN_STATUS_HYPHEN_INDEX))
				throw new MessageException("Hangman status field invalid!");
		}
		if(hangmanStatusField.charAt(HANGMAN_STATUS_INCORRECT_GUESSES_INDEX) != 
				hangmanStatusField.charAt(HANGMAN_STATUS_MAX_INCORRECT_GUESSES_INDEX))
			throw new MessageException("Hangman status field invalid!");
			
		this.hangmanStatusField = hangmanStatusField;
	}
}
