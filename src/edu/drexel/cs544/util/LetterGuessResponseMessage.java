package edu.drexel.cs544.util;

/**
 * Definition for Letter Guess Response Message.
 * 
 * @author Brian Wilhelm
 * 
 */
public class LetterGuessResponseMessage extends HangmanMessage {

	public LetterGuessResponseMessage() {
		super(MessageType.LETTER_GUESS_RESPONSE);

	}

	public LetterGuessResponseMessage(Double version)
			throws MessageException {
		super(MessageType.LETTER_GUESS_RESPONSE, version);

	}

	public LetterGuessResponseMessage(Double version, Integer sequence) 
			throws MessageException {
		super(MessageType.LETTER_GUESS_RESPONSE, version, sequence);

	}
	
	public LetterGuessResponseMessage(Double version, Integer sequence, String dataField) 
			throws MessageException {
		super(MessageType.LETTER_GUESS_RESPONSE, version, sequence, dataField);

	}
	
	public LetterGuessResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField) 
			throws MessageException {
		super(MessageType.LETTER_GUESS_RESPONSE, version, sequence, dataField, 
				solutionStatusField);

	}
	
	public LetterGuessResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField) 
			throws MessageException {
		super(MessageType.LETTER_GUESS_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField);

	}
	
	public LetterGuessResponseMessage(Double version, Integer sequence, String dataField,
			String solutionStatusField, String hangmanStatusField, String gameOverField) 
			throws MessageException {
		super(MessageType.LETTER_GUESS_RESPONSE, version, sequence, dataField, 
				solutionStatusField, hangmanStatusField, gameOverField);

	}
	
	@Override
	public void setDataField(String dataField) 
			throws MessageException {
		
		if (!dataField.equals(POSITIVE) && !dataField.equals(NEGATIVE))
			throw new MessageException("Data field invalid!");
		
		this.dataField = dataField;
	}
}
