package edu.drexel.cs544.util.test;

import java.util.*;

import edu.drexel.cs544.util.*;

/**
 * Tests the Hangman Message Factory.
 * 
 * @author Brian Wilhelm
 * 
 */
public class TestHangmanMessageFactory {
	
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		
		Map<String,Boolean> results = new HashMap<String,Boolean>();
		
		results.put("ConnectMessage",               TestEncodeDecodeConnectMessage());
		results.put("GameRequestMessage",           TestEncodeDecodeGameRequestMessage());
		results.put("LetterGuessMessage",           TestEncodeDecodeLetterGuessMessage());
		results.put("SolutionGuessMessage",         TestEncodeDecodeSolutionGuessMessage());
		results.put("EndGameMessage",               TestEncodeDecodeEndGameMessage());
		results.put("DisconnectMessage",            TestEncodeDecodeDisconnectMessage());
		results.put("ConnectResponseMessage",       TestEncodeDecodeConnectResponseMessage());
		results.put("GameRequestResponseMessage",   TestEncodeDecodeGameRequestResponseMessage());
		results.put("LetterGuessResponseMessage",   TestEncodeDecodeLetterGuessResponseMessage());
		results.put("SolutionGuessResponseMessage", TestEncodeDecodeSolutionGuessResponseMessage());
		results.put("EndGameResponseMessage",       TestEncodeDecodeEndGameResponseMessage());
		results.put("DisconnectResponseMessage",    TestEncodeDecodeDisconnectResponseMessage());
		results.put("ErrorMessage",    			    TestEncodeDecodeErrorMessage());
		
        Iterator it = results.entrySet().iterator();
        while(it.hasNext())
        {
            Map.Entry m =(Map.Entry)it.next();
            System.out.println(m.getKey() + ": " + m.getValue());
        }
	}

	public static boolean AreMessagesEqual(Message messageOne, Message messageTwo) {
		
		try {
			if(messageOne.getMessageType() != messageTwo.getMessageType() || 
			   !messageOne.getVersion().equals(messageTwo.getVersion()) || 
			   !messageOne.getSequence().equals(messageTwo.getSequence()))
				return false;

			if(messageOne.getMessageType().usesDataField() &&
			   !messageOne.getDataField().equals(messageTwo.getDataField()))
				return false;
			
			if(messageOne.getMessageType().usesSolutionStatusField() &&
			   !messageOne.getSolutionStatusField().equals(messageTwo.getSolutionStatusField()))
				return false;
			
			if(messageOne.getMessageType().usesHangmanStatusField() &&
			   !messageOne.getHangmanStatusField().equals(messageTwo.getHangmanStatusField()))
				return false;
			
			if(messageOne.getMessageType().usesGameOverField() &&
			   (messageOne.isGameOverFieldSet() != messageTwo.isGameOverFieldSet() ||
			    (messageOne.isGameOverFieldSet() == true && 
			     !messageOne.getGameOverField().equals(messageTwo.getGameOverField()))))
				return false;
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
			return false;
		}
		
		return true;
	}
	
	public static boolean TestEncodeDecodeConnectMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new ConnectMessage(1.0, 0);
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeGameRequestMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new GameRequestMessage(1.0, 45, "RES");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeLetterGuessMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new LetterGuessMessage(1.1, 23, "A");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeSolutionGuessMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new SolutionGuessMessage(1.0, 123, "WORDS");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeEndGameMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new EndGameMessage(2.0, 30);
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeDisconnectMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new DisconnectMessage(1.0, 999);
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeConnectResponseMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new ConnectResponseMessage(1.0, 1, "NEG");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeGameRequestResponseMessage() {
		
		boolean return_value = false;
		
		try {
			GameRequestResponseMessage original_message_object = new GameRequestResponseMessage(1.0, 45, "NEG");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeLetterGuessResponseMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new LetterGuessResponseMessage(1.0, 45, "POS", "WO-DS", "4-6");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeSolutionGuessResponseMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new SolutionGuessResponseMessage(1.0, 45, "NEG", "WO-DS", "6-6", "LOSE");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeEndGameResponseMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new EndGameResponseMessage(1.0, 45, "POS", "WORDS", "6-6");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeDisconnectResponseMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new DisconnectResponseMessage(1.0, 999);
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
	
	public static boolean TestEncodeDecodeErrorMessage() {
		
		boolean return_value = false;
		
		try {
			Message original_message_object = new ErrorMessage(1.0, 50, "100-Missing Data Field");
			String encoded_message_string = HangmanMessageFactory.encode(original_message_object);
			Message decoded_message_object = (HangmanMessage)HangmanMessageFactory.decode(encoded_message_string);
			return_value = AreMessagesEqual(original_message_object, decoded_message_object);
		}
		catch(MessageException exp) {
			System.out.println(exp.getMessage());
		}
		
		return return_value;
	}
}
