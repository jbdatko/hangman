package edu.drexel.cs544.gui;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.screens.Quit;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Abstract console driven menu for simple games
 * 
 * @author Josh Datko
 * 
 */
public abstract class ConsoleScreen implements Screen {

	/**
	 * Number of rows in the terminal
	 */
	private static final int NUMBER_OF_ROWS = 30;

	/**
	 * Menu of selectable screens from this screen
	 */
	protected HashMap<Integer, Screen> menu = new HashMap<Integer, Screen>();

	/**
	 * The Hangman game interface
	 */
	private HangmanGame game;

	/**
	 * @return the hangman game interface
	 */
	protected final HangmanGame getGame() {
		return game;

	}

	/**
	 * Obtain the output stream
	 * 
	 * @return the output stream to use
	 */

	public ConsoleScreen(HangmanGame game) {
		this.game = game;
		this.addExitMenu();
	}

	/**
	 * @return the output stream for a console
	 */
	private final PrintStream getOut() {
		return System.out;
	}

	/**
	 * Get the scanner object for inputs
	 * 
	 * @return the Scanner on the dedicated input stream
	 */
	protected final Scanner getScanner() {
		return new Scanner(System.in);
	}

	/**
	 * Utility method to add the quit menu to position zero
	 */
	protected void addExitMenu() {
		this.menu.put(0, new Quit(this.getGame()));
	}

	/**
	 * Prints out all screens reachable from this screen
	 */
	protected void printMenu() {

		this.print("Menu:");

		PriorityQueue<Integer> q = new PriorityQueue<Integer>(
				this.menu.keySet());

		while (!q.isEmpty())
			this.print(q.peek() + ") " + this.menu.get(q.poll()).toString());

	}

	/**
	 * Get user's valid menu choice
	 * 
	 * @return A validated (i.e. menu exists) menu choice
	 */
	protected int getMenuChoice() {
		int choice = -1;

		while (choice < 0) {
			try {
				choice = this.getScanner().nextInt();
				if (!this.menu.containsKey(choice))
					throw new InputMismatchException();
			} catch (InputMismatchException e) {

				choice = this.printInvalidInput();
			}
		}

		return choice;
	}

	/**
	 * Print out invalid entry, prompt user to continue
	 * 
	 * @return -1
	 */
	private int printInvalidInput() {
		this.print("Invalid entry.");

		this.getEnterKey();

		this.clear();
		this.draw();
		return -1;

	}

	/**
	 * Prompt user for the ENTER key and wait for input. Discard anything
	 * entered, only care if ENTER was pressed.
	 */
	protected void getEnterKey() {
		this.print("Press ENTER to continue");
		try {
			new InputStreamReader(System.in).read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Return the selected screen
	 * 
	 * @return the screen associated with the users selection
	 */
	protected Screen getMenuScreen() {
		return this.menu.get(this.getMenuChoice());
	}

	/**
	 * Clear the terminal
	 */
	protected void clear() {

		for (int rows = 0; rows < NUMBER_OF_ROWS; rows++)
			this.getOut().printf("\n");

	}

	/**
	 * Print out to the terminal and add a new line
	 * 
	 * @param s
	 *            the string to print
	 */
	protected void print(String s) {
		this.getOut().println(s);
	}

	/**
	 * Utility method to add the screen to the next spot in the menu
	 * @param s the screen to add
	 * @return true if it was added otherwise, false
	 */
	protected boolean addScreen(Screen s) {

		if (!this.menu.containsValue(s)) {

			for (int x = 0; x < 100; x++) {
				if (!this.menu.containsKey(x)) {
					this.menu.put(x, s);
					return true;
				}
				

			}

		}
		
		return false;
	}

}
