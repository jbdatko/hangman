package edu.drexel.cs544.gui.screens;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.net.TwitterServerDiscovery;


/**
 * Defines the initial welcome screen
 * @author Josh Datko
 *
 */
public class Welcome extends ConsoleScreen {
	
	/**
	 * The Hangman Logo
	 */
	public static final String logo = "    )\n" +
			              " ( /(\n" +
					      " )\\())   )       (  (     )      )\n" +
			              "((_)\\ ( /(  (    )\\))(   (    ( /(  (\n" +
					      " _((_))(_)) )\\ )((_))\\   )\\  ')(_)) )\\ )\n" +
			              "| || ((_)_ _(_/( (()(_)_((_))((_)_ _(_/(\n" +
					      "| __ / _` | ' \\)) _` || '  \\() _` | ' \\))\n" +
			              "|_||_\\__,_|_||_|\\__, ||_|_|_|\\__,_|_||_|\n" +
					      "                |___/\n";

	/**
	 * Creates the welcome screen
	 * @param game the game interface
	 */
	public Welcome(HangmanGame game) {
		super(game);
		
		TwitterServerDiscovery discover = new TwitterServerDiscovery();
		
		this.menu.put(1, new About(this, game));
		this.menu.put(2, new LaunchHangman(this, game));
		
		
	}

	@Override
	public void draw() {
		
		this.clear();
		this.print("Welcome to HangMan!!");

			              
		System.out.println(logo);
		this.printMenu();


	}

	@Override
	public Screen execute() {
		
		return this.getMenuScreen();
	}
	
	@Override
	public String toString() {
		return "Welcome Screen";
	}

}
