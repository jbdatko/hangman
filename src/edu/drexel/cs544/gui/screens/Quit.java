package edu.drexel.cs544.gui.screens;

import java.net.SocketException;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;

/**
 * Prints the quit screen
 * 
 * @author Josh Datko
 * 
 */
public class Quit extends ConsoleScreen {

	public Quit(HangmanGame game) {
		super(game);

	}

	@Override
	public void draw() {

		this.clear();
		this.print("Bye!");

	}

	@Override
	public Screen execute() {

		try {
			this.getGame().endGame();
		} catch (SocketException e) {
			
			this.print(e.getMessage() + ": performing a hard shutdown");
			
		}

		System.exit(0);

		return null;
	}

	@Override
	public String toString() {
		return "Quit";
	}

	@Override
	protected void addExitMenu() {
		// Don't add the exit menu to itself... bad things happen...
	}

}
