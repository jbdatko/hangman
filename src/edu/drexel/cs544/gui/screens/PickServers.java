package edu.drexel.cs544.gui.screens;

import java.util.List;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.net.TwitterServerDiscovery;

/**
 * Screen which displays available game servers
 * 
 * @author Josh Datko
 * 
 */
public class PickServers extends ConsoleScreen {

	/**
	 * Inner class to display server names
	 * 
	 * @author Josh Dakto
	 * 
	 */
	public class ServerName extends ConsoleScreen {

		/**
		 * The server's name
		 */
		private String server;

		/**
		 * Create ther server name screen
		 * 
		 * @param name
		 *            server name
		 * @param g
		 *            game interface
		 */
		public ServerName(String name, HangmanGame g) {
			super(g);
			this.server = name;

		}

		@Override
		public String toString() {
			return this.server;
		}

		@Override
		public void draw() {
			// Nothing to do

		}

		@Override
		public Screen execute() {

			return this;

		}

	}

	/**
	 * Create the pick server screen
	 * 
	 * @param g
	 *            the game interface
	 */
	public PickServers(HangmanGame g) {
		super(g);

	}

	@Override
	public void draw() {
		// Create a twitter discovery instance
		TwitterServerDiscovery d = new TwitterServerDiscovery();

		// Grab a list of servers, may be empty
		List<String> l = d.getServers();

		int num = 1;

		for (String s : l) {
			this.menu.put(num, new ServerName(s, this.getGame()));
			num++;
		}

		this.clear();
		this.print("The following game servers were discovered, choose one:");
		this.printMenu();

	}

	@Override
	public Screen execute() {

		return this.getMenuScreen();

	}

	@Override
	public String toString() {
		return "Discover and pick a game server";
	}

}
