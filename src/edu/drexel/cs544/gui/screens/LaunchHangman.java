package edu.drexel.cs544.gui.screens;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.gui.screens.PickServers.ServerName;

/**
 * Launch a new Hangman Game screen
 * 
 * @author Josh Datko
 * 
 */
public class LaunchHangman extends ConsoleScreen {

	private Screen mainMenu;

	/**
	 * Creates a new launch screen
	 * 
	 * @param mainMenu
	 *            the main menu, so the screen can return
	 * @param game
	 *            the game interface
	 */
	public LaunchHangman(Screen mainMenu, HangmanGame game) {
		super(game);

		this.mainMenu = mainMenu;

		this.menu.put(1, new PickServers(game));

	}

	@Override
	public void draw() {
		this.clear();
		this.print("Choose the game option:");
		this.printMenu();

	}

	@Override
	public Screen execute() {

		Screen s = this.getMenuScreen();

		if (PickServers.class.isInstance(s)) {
			s.draw();
			s = s.execute();

			// replace this screen with the chosen server
			if (ServerName.class.isInstance(s)) {

				// Start the game
				if (null != s.execute()) {
					// Success, replace the menu option
					this.menu.put(1, new PlayHangman(this.getGame(), mainMenu,
							s.toString()));
				}
				else{
					//Failure, so allow the user to connect again
				}

				return this;
			}

		}

		return s;
	}

	@Override
	public String toString() {
		return "Play Hangman!";
	}

}
