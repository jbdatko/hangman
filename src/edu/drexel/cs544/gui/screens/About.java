package edu.drexel.cs544.gui.screens;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.game.Versions;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;

/**
 * About Screen to show team members
 * 
 * @author Josh Datko
 * 
 */
public class About extends ConsoleScreen {

	public final static String ABOUT = "                         ''~``\n"
			+ "                        ( o o )\n"
			+ "+------------------.oooO--(_)--Oooo.------------------+\n"
			+ "|                Game Designed by Group 8             |\n"
			+ "|             For Drexel University's CS544           |\n"
			+ "|   Arindam Biswas, Joshua Datko, Alexander Duff and  |\n"
			+ "|                    Brian Wilhelm                    |\n"
			+ "|                    .oooO                            |\n"
			+ "| Version " + Versions.HANGMAN_VERSION
			             + "         (   )   Oooo.                   |\n"
			+ "+---------------------\\ (----(   )--------------------+\n"
			+ "                       \\_)    ) /\n"
			+ "                             (_/\n";

	/**
	 * Create the About screen
	 * 
	 * @param welcomeScreen
	 *            Pointer to the welcome screen, so we can return
	 */
	About(Screen welcomeScreen, HangmanGame game) {
		super(game);

		this.menu.put(1, welcomeScreen);

	}

	@Override
	public void draw() {

		this.clear();

		this.print(ABOUT);

		this.printMenu();

	}

	@Override
	public Screen execute() {

		return this.getMenuScreen();
	}

	@Override
	public String toString() {
		return "About";
	}

}
