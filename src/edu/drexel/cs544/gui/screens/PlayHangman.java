package edu.drexel.cs544.gui.screens;

import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.server.game.GameState;

/**
 * The main game screen
 * 
 * @author Josh Datko
 * 
 */
public class PlayHangman extends ConsoleScreen {

	/**
	 * The main menu screen to which this game can return
	 */
	private Screen mainMenu;

	private String serverName;
	
	private Set<String> cachedGuesses = new HashSet<String>();

	/**
	 * Create the Game screen
	 * 
	 * @param g
	 *            the game interface
	 * @param mainMenu
	 *            the main menu, so that we can return
	 * @param serverName
	 *            the Name of the server to connect to
	 */
	public PlayHangman(HangmanGame g, Screen mainMenu, String serverName) {
		super(g);

		this.mainMenu = mainMenu;
		this.serverName = serverName;
		

		this.menu.put(1, new GuessScreen(this, g, this.cachedGuesses));

	}

	@Override
	public void draw() {

		this.clear();
		this.print(Welcome.logo);
		this.printGameInfo();

		this.printMenu();

	}

	/**
	 * Prints the status and info lines
	 */
	public void printGameInfo() {
		this.print("Puzzle Status:");
		this.print("** Example: p-zzl- : 2-6 **");
		this.print("** Means: 2 Incorrect Guess - Out of 6 Allowed **");
		this.print("Your puzzle Status currently is: "
				+ this.getGame().getPuzzleStatus() + " : "
				+ this.getGame().usedGuesses() + "-" + this.getGame().guesses());
	}

	@Override
	public Screen execute() {

		Screen returnScreen = this;
		this.cachedGuesses.clear();

		this.clear();

		this.print("Connecting to " + this + ".  Please wait...");

		boolean connected = true;

		try {
			connected = this.getGame().startGame(this.toString());
		} catch (SocketException e) {
			this.print("Socket Exception");
		} finally {
			if (connected) {
				this.print("Connected!");
				this.getEnterKey();
				this.draw();

			} else {
				this.print("Connection Failed!");
				this.getEnterKey();
				try {
					this.getGame().endGame();
				} catch (SocketException e) {
					
				}
				return this.mainMenu;
			}

		}
		
		

		while (true) {
			Screen s = this.getMenuScreen();

			s.draw();
			s.execute();
			this.draw();

			if (this.getGame().getState() == GameState.WON) {
				this.print("You won!");
				this.print("The word was: " + this.getGame().correctPhrase());
				this.print("Thanks for playing!");
				try {
					this.getGame().endGame();
				} catch (SocketException e) {
					this.print("Socket Error: " + e.getMessage());
					this.getEnterKey();
					this.menu.get(0).draw();
					this.menu.get(0).execute();
					
				}
				this.getEnterKey();
				returnScreen = this.mainMenu;
				break;
			}

			else if (this.getGame().getState() == GameState.LOST) {
				System.out
						.println("So you get NOTHING! You LOSE!  GOOD DAY SIR!");

				this.print("Play again if you dare.");
				try {
					this.getGame().endGame();
				} catch (SocketException e) {
					this.print("Socket Error: " + e.getMessage());
					this.getEnterKey();
					this.menu.get(0).draw();
					this.menu.get(0).execute();
					
				}
				this.getEnterKey();
				returnScreen = this.mainMenu;
				break;
			}
		}

		return returnScreen;

	}

	@Override
	public String toString() {
		//Use just the server name since this will be passed into client
		return serverName;
	}

}
