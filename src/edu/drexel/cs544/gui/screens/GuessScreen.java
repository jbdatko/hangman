package edu.drexel.cs544.gui.screens;

import java.io.IOException;
import java.net.SocketException;
import java.util.InputMismatchException;
import java.util.Set;
import java.util.regex.Pattern;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.server.game.Guess;

/**
 * Guess screen to get user guess for Hangman
 * 
 * @author Josh Datko
 * 
 */
public class GuessScreen extends ConsoleScreen {

	/**
	 * The new game screen, which we will return to after a game over
	 */
	private PlayHangman gameScreen;

	/**
	 * Longest Hangman word
	 */
	private static final int LONGEST_WORD = "superfragilisticexpialidocious"
			.length();
	/**
	 * Regexp for correctly formatted guesses
	 */
	private static Pattern word = Pattern.compile("^\\p{Alpha}\\p{Alpha}*$");
	
	/**
	 * Cached the guesses to prevent the user from guessing the same thing twice
	 */
	private Set<String> cachedGuesses;

	/**
	 * h Create a guess screen for hangman
	 * 
	 * @param hm
	 *            the play hangman screen, which this screen will return on game
	 *            over
	 * @param g
	 *            the game interface
	 * @param cachedGuesses 
	 */
	public GuessScreen(PlayHangman hm, HangmanGame g, Set<String> c) {
		super(g);
		this.gameScreen = hm;
		this.cachedGuesses = c;
	}

	@Override
	public void draw() {
		this.clear();
		this.print(Welcome.logo);
		this.gameScreen.printGameInfo();
		this.print("Guess letter or word:");

	}

	@Override
	public Screen execute() {

		String result = null;
		while (null == result) {
			try {
				//Get the user input
				result = this.getScanner().next(GuessScreen.word);
				if (result.length() > LONGEST_WORD) {
					result = null;
					throw new IOException("You must enter either a alpha-character or a word that is no longer than "
						+ LONGEST_WORD + " characters");
				}
				
				//Convert the guess to upper case
				result = result.toUpperCase();
				
				if (!this.cachedGuesses.add(result)){
					//Already guessed
					result = null;
					throw new IOException("Sorry, you already guessed that.  Please try again");
					
				}
			} catch (IOException e) {
				this.print(e.getMessage());
				this.getEnterKey();
				this.draw();
			} catch (InputMismatchException e){
				this.print("Enter either a letter or a word");
				this.getEnterKey();
				this.draw();
			}
		}

		Guess g = null;
		
		this.print("Guessing... please wait...");
		
		try {
			if (result.length() == 1) {
				// Guess was a character
				g = new Guess(result.charAt(0));

				this.getGame().processGuess(g);
			} else if (result.length() == 0) {
				throw new NullPointerException("zero length word");
			} else {
				// Guess was a word
				g = new Guess(result);

				this.getGame().processGuess(g);
			}
		} catch (SocketException e) {
			
			this.print("Socket Exception " + e.getMessage());
			this.print("Ending game");
			this.getEnterKey();
			
			//Grab the exit screen and quit
			this.menu.get(0).draw();
			this.menu.get(0).execute();

		}
		this.printGuessStatus(g);

		return null;

	}

	@Override
	public String toString() {
		return "Make Guess";
	}

	/**
	 * Utility method to print out the result of the last guess
	 * 
	 * @param g
	 *            the guess
	 */
	private void printGuessStatus(Guess g) {
		if (null == g)
			throw new NullPointerException("Guess can't be null");

		if (g.getResult()) {
			this.print("CORRECT!");
			this.getEnterKey();
		} else {
			this.print("INCORRCT!");
			this.getEnterKey();
		}
	}

}
