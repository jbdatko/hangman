package edu.drexel.cs544.gui;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.screens.Welcome;

/**
 * Creates the thread for the Hangman GUI
 * @author Josh Datko
 *
 */
public class HangmanGUI implements Runnable{
	
	/**
	 * The hangman game interface
	 */
	private HangmanGame game;
	
	/**
	 * Create the GUI and spawn the GUI thread
	 * @param game the game interface
	 */
	public HangmanGUI (HangmanGame game){
		if ( null == game)
			throw new NullPointerException("Game can't be null");
		
		this.game = game;
		
		Thread tr = new Thread(this, "Hangman GUI");
		
		tr.start();
	}

	@Override
	public void run() {
		
		Screen s = new Welcome(this.game);
		
		while (true){
			s.draw();
			s = s.execute();
		}
		
	}

}
