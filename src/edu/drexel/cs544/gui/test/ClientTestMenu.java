package edu.drexel.cs544.gui.test;

import java.net.Socket;

import javax.net.SocketFactory;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.ConsoleScreen;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;
import edu.drexel.cs544.net.TwitterSocketFactory;

/**
 * A test screen for testing
 * @author Josh Datko
 *
 */
public class ClientTestMenu extends ConsoleScreen {

	public ClientTestMenu(Screen mainMenu, HangmanGame g) {
		super(g);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw() {
		this.clear();
		this.print("Secret test menu:");

	}
	
	@Override
	public String toString(){
		return "Secret Test Menu, will be removed for final version";
	}

	@Override
	public Screen execute() {
		
		this.print("Enter servername");
		
		String serverName = this.getScanner().next();
		
		SocketFactory fac = TwitterSocketFactory.getInstance();

		try {
			Socket soc = fac.createSocket(serverName, 0);

			TwitterOutputStream out = (TwitterOutputStream) soc
					.getOutputStream();
			
			TwitterInputStream in = (TwitterInputStream) soc.getInputStream();
			
			this.print("Connected!");
			
			while (true) {
				this.print("Enter tweet:");

				String tweet = this.getScanner().next();
				
				if (tweet.contains("quit"))
					break;

				out.write(tweet);
				this.print("Server Responded: " + in.read_wait());

			}			
			
			soc.close();
			
			this.print("Closed!");
			
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this;
	}

}
