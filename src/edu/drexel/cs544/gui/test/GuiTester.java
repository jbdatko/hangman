package edu.drexel.cs544.gui.test;

import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.gui.HangmanGUI;
import edu.drexel.cs544.gui.Screen;
import edu.drexel.cs544.gui.screens.Welcome;
import edu.drexel.cs544.server.game.Game;

import java.io.Console;

public class GuiTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//For the client, this should be the client controller
		HangmanGame game = new Game("", 6);
		
		//Launch the GUI (spawns a gui thread)
		new HangmanGUI(game);
		

	}

}
