package edu.drexel.cs544.gui;

/**
 * Basic screen interface for a GUI
 * @author Josh Datko
 *
 */
public interface Screen {
	
	/**
	 * Draw the screen to the display
	 */
	public void draw();
	
	/**
	 * Execute this screen
	 * @return the next screen
	 */
	public Screen execute();

}
