package edu.drexel.cs544.server;

public interface DFAState {

	public static final int CLOSED = 1;
	
	public static final int CONN_INIT = 2;
	
	public static final int RDY_FOR_GAME = 3;
	
	public static final int GAME_INIT = 4;
	
	public static final int RDY_TO_PLAY = 5;
	
	public static final int SOL_GUESSED = 6;
	
	public static final int LTR_GUESSED = 7;
	
	public static final int GAME_END_INIT = 8;

}
