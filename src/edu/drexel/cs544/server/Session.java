package edu.drexel.cs544.server;

import edu.drexel.cs544.server.game.Game;
import edu.drexel.cs544.server.game.GameState;
import edu.drexel.cs544.server.game.Guess;
import edu.drexel.cs544.util.ConnectResponseMessage;
import edu.drexel.cs544.util.DisconnectResponseMessage;
import edu.drexel.cs544.util.EndGameResponseMessage;
import edu.drexel.cs544.util.ErrorMessage;
import edu.drexel.cs544.util.GameRequestResponseMessage;
import edu.drexel.cs544.util.HangmanMessage;
import edu.drexel.cs544.util.HangmanMessageFactory;
import edu.drexel.cs544.util.LetterGuessResponseMessage;
import edu.drexel.cs544.util.MessageException;
import edu.drexel.cs544.util.SolutionGuessResponseMessage;

public class Session {

	private int state;
	private double versionNumber = 1.0;
	private static int numberOfSessions = 0;
	private int sequenceNumber;
	private String inboundCommand;
	private int expectedInboundSequenceNumber;
	private HangmanMessage inboundMessage = null;
	private HangmanMessage outBoundMessage = null;
	private Game game = null;

	// for testing
	char letterGuess;
	String phraseGuess;

	/**
	 * @return
	 * @description constructor that initializes variables
	 */

	public Session() {

		this.state = DFAState.CLOSED;
		this.sequenceNumber = -1;
		this.expectedInboundSequenceNumber = 0;
	}

	/**
	 * @return
	 * @description returns the current state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param msg
	 * @return
	 * @throws Exception
	 * @description main entry point for session class
	 * 
	 */
	public String processSessionRequest(String message) {

		String stringOutboundMessage = null;
		boolean isMalformedMessage = false;

		// process the actual message
		process: {

			try {

				// decode message
				inboundMessage = (HangmanMessage) HangmanMessageFactory
						.decode(message.trim());
				inboundCommand = inboundMessage.getMessageType().getKeyword();

				// check if the message is out of sequence
				if (isOutOfSequence())
					break process;

				// check if the message is out of order
				if (isOutOfOrder())
					break process;

				// execute the DFA
				executeDFA();
				
			} catch (MessageException e) {
				// TODO Auto-generated catch block
				isMalformedMessage = true;
				break process;

			}

		}

		// if malformed message during process create one otherwise try encoding
		if (isMalformedMessage)

			stringOutboundMessage = processMalformedMessage();

		else {
			try {
				
				// set the common message variables
				setCommonMessageVariables();
				// encode the message
				stringOutboundMessage = HangmanMessageFactory
						.encode(outBoundMessage);

				// increment the next expected sequence number
				expectedInboundSequenceNumber = expectedInboundSequenceNumber + 2;

				// reset the outound message
				outBoundMessage = null;
			} catch (MessageException e) {
				stringOutboundMessage = processMalformedMessage();
			}

		}

		return stringOutboundMessage;

	}

	public void setTestVariables(int sequenceNumber, String command,
			int numberOfSessions, char letterGuess, String phraseGuess) {

		this.inboundCommand = command;
		Session.numberOfSessions = numberOfSessions;
		this.letterGuess = letterGuess;
		this.phraseGuess = phraseGuess;
	}

	/**
	 * @throws MessageException
	 * @description Executes the protocol DFA
	 * 
	 */
	private void executeDFA() throws MessageException {

		// state machine

		// check if disconnect request
		if (inboundCommand.equalsIgnoreCase(MessageCommand.DISC)) {

			outBoundMessage = new DisconnectResponseMessage();
			disconnect();

			// save the game here
		} else {
			// process DFA as usual

			switch (state) {

			case (DFAState.CLOSED):
				processConnectionRequest();
				break;

			case (DFAState.CONN_INIT):
				connect();
				break;

			case (DFAState.RDY_FOR_GAME):
				processGameRequest();
				break;

			case (DFAState.GAME_INIT):
				establishGame();
				break;

			case (DFAState.RDY_TO_PLAY):
				processMove();
				break;

			case (DFAState.LTR_GUESSED):
				processLetterGuess();
				break;

			case (DFAState.SOL_GUESSED):
				processSolutionGuess();
				break;

			case (DFAState.GAME_END_INIT):
				// end the game here
				processGameEnd();
				state = DFAState.RDY_FOR_GAME;
				break;

			}
		}
	}

	/**
	 * @description checks if the message is out of sequence
	 * @return
	 * @throws MessageException
	 * @throws
	 */
	private boolean isOutOfSequence() throws MessageException {

		boolean rtnVal = false;

		// check if message is out of sequence,and immediately disconnect
		// and go back to a closed state

		if (expectedInboundSequenceNumber != Integer.parseInt(inboundMessage
				.getSequence())) {

			ErrorMessage msgLocal = new ErrorMessage();
			outBoundMessage = msgLocal;

			msgLocal.setDataField("200-Message Out of Sequence");
			
			state = DFAState.CLOSED;
			rtnVal = true;
		}

		return rtnVal;
	}

	/**
	 * @description checks if the message is out of order
	 * @return
	 * @throws MessageException
	 */
	private boolean isOutOfOrder() throws MessageException {

		boolean rtnVal = false;
		// check if message is out of order, and immediately disconnect and
		// go back to a closed state
		if (MessageValidator.isMessageOutOfOrder(inboundCommand, state)) {

			ErrorMessage msgLocal = new ErrorMessage();
			outBoundMessage = msgLocal;

			msgLocal.setDataField("300-Message Out of Order");
			state = DFAState.CLOSED;
			rtnVal = true;
		}

		return rtnVal;

	}

	/**
	 * @description processes a malformed message to create the appropriate
	 *              response message
	 */
	private String processMalformedMessage() {

		// reset the existing message and create the outbound malformed
		// message error
		outBoundMessage = null;
		outBoundMessage = new ErrorMessage();

		String malformedMessage = "";

		try {
			outBoundMessage.setDataField("100-Missing Data Field");
			setCommonMessageVariables();
			malformedMessage = HangmanMessageFactory.encode(outBoundMessage);

		} catch (MessageException e) {

			e.printStackTrace();
			malformedMessage = "HM/" + versionNumber + "/" + sequenceNumber
					+ "/ERRO/" + "100-Missing Data Field//";

		}

		state = DFAState.CLOSED;

		return malformedMessage;
	}

	/**
	 * @throws MessageException
	 * @description sets common message variables such as sequence number,
	 *              version
	 */
	private void setCommonMessageVariables() throws MessageException {

		// increment the sequence number
		sequenceNumber = sequenceNumber + 2;
		outBoundMessage.setSequence(sequenceNumber);
		outBoundMessage.setVersion(versionNumber);

	}

	/**
	 * @throws MessageException
	 * @description sets common game repsonse message variables such as phrase
	 *              status and solution status
	 */
	private void setCommonGameMessageVariables() throws MessageException {

		HangmanMessage msgLocal = (HangmanMessage) outBoundMessage;
		msgLocal.setHangmanStatusField(game.usedGuesses() + "-"
				+ game.guesses());
		msgLocal.setSolutionStatusField(game.getPuzzleStatus());

	}

	/**
	 * @throws MessageException
	 * @description processes the initial connection request
	 */
	private void processConnectionRequest() throws MessageException {

		// transition states and move forward in the DFA (e.g., establish
		// connection)
		state = DFAState.CONN_INIT;
		executeDFA();

	}

	/**
	 * @throws MessageException
	 * @description method establishes the actual connection for the session
	 * 
	 */
	private void connect() throws MessageException {

		// if less than 5 games allow session to be created\
		ConnectResponseMessage msgLocal = new ConnectResponseMessage();
		outBoundMessage = msgLocal;

		if (getNumberOfSessions() < 5) {

			incrementNumberOfSessions();
			state = DFAState.RDY_FOR_GAME;
			msgLocal.setDataField(msgLocal.POSITIVE);

		} else {

			state = DFAState.CLOSED;
			msgLocal.setDataField(msgLocal.NEGATIVE);
		}
	}

	/**
	 * @throws MessageException
	 * @description method for processing the game request command
	 * 
	 */
	private void processGameRequest() throws MessageException {

		// transition states and move forward in the DFA (e.g., create/continue
		// game)
		state = DFAState.GAME_INIT;

		executeDFA();
	}

	/**
	 * @throws MessageException
	 * @description method creates/continues a game
	 * 
	 */
	private void establishGame() throws MessageException {

		// assume game exists for now
		GameRequestResponseMessage msgLocal = new GameRequestResponseMessage();
		outBoundMessage = msgLocal;

		if (inboundMessage.getDataField().equalsIgnoreCase("NEW")) {

			game = new Game("", 4);
			state = DFAState.RDY_TO_PLAY;
			msgLocal.setDataField(msgLocal.POSITIVE);
			setCommonGameMessageVariables();

		} else if (inboundMessage.getDataField().equalsIgnoreCase("RES")) {

			// for future implementations, resume game will just deny

			state = DFAState.RDY_FOR_GAME;
			msgLocal.setDataField(msgLocal.NEGATIVE);

		}

	}

	private void processMove() throws MessageException {

		if (inboundCommand.equalsIgnoreCase(MessageCommand.ENDG))
			state = DFAState.GAME_END_INIT;

		// if letter guess set next state and execute DFA
		if (inboundCommand.equalsIgnoreCase(MessageCommand.LGUE))
			state = DFAState.LTR_GUESSED;

		// if solution guess set next state and execute DFA
		if (inboundCommand.equalsIgnoreCase(MessageCommand.SGUE))
			state = DFAState.SOL_GUESSED;

		executeDFA();
	}

	/**
	 * @throws MessageException
	 * @description method for processing the letter guess command
	 * 
	 */
	private void processLetterGuess() throws MessageException {

		if (!inboundCommand.equalsIgnoreCase(MessageCommand.ENDG)) {

			// process the solution guess
			Guess guess = new Guess(inboundMessage.getDataField().charAt(0));
			game.processGuess(guess);

			// letter guess response message
			LetterGuessResponseMessage msgLocal = new LetterGuessResponseMessage();
			outBoundMessage = msgLocal;

			// if correct guess set positive otherwise negative respose on the
			// message
			if (guess.getResult()) {
				msgLocal.setDataField(msgLocal.POSITIVE);
			} else {
				msgLocal.setDataField(msgLocal.NEGATIVE);
			}

			// check game state to set the state for DFA
			if (game.getState() == GameState.LOST) {

				state = DFAState.RDY_FOR_GAME;
				msgLocal.setGameOverField(msgLocal.LOSE);

			} else if (game.getState() == GameState.WON) {

				// check game state to set the state for DFA
				state = DFAState.RDY_FOR_GAME;
				msgLocal.setGameOverField(msgLocal.WIN);

			} else {

				// continue playing
				state = DFAState.RDY_TO_PLAY;

			}

			// set other game message fields
			setCommonGameMessageVariables();

		} else {

			state = DFAState.GAME_END_INIT;
			executeDFA();
		}

	}

	/**
	 * @throws MessageException
	 * @description method for processing the solution guess command
	 * 
	 */
	private void processSolutionGuess() throws MessageException {

		// process the letter guess
		Guess guess = new Guess(inboundMessage.getDataField());
		game.processGuess(guess);

		// solution guess response message
		SolutionGuessResponseMessage msgLocal = new SolutionGuessResponseMessage();
		outBoundMessage = msgLocal;

		// if correct guess set positive otherwise negative response on the
		// message
		if (guess.getResult()) {
			msgLocal.setDataField(msgLocal.POSITIVE);
		} else {
			msgLocal.setDataField(msgLocal.NEGATIVE);
		}

		// if there is no end game message, process as normal
		if (!inboundCommand.equalsIgnoreCase(MessageCommand.ENDG)) {

			if (game.getState() == GameState.LOST) {

				// end the game and go back to rdy for game
				state = DFAState.RDY_FOR_GAME;
				msgLocal.setGameOverField(msgLocal.LOSE);

			} else if (game.getState() == GameState.WON) {

				// end the game and go back to rdy for game
				state = DFAState.RDY_FOR_GAME;
				msgLocal.setGameOverField(msgLocal.WIN);

			} else {
				// continue playing
				state = DFAState.RDY_TO_PLAY;

			}

			// set other game message fields
			setCommonGameMessageVariables();

		} else {
			state = DFAState.GAME_END_INIT;
			executeDFA();
		}
	}

	/**
	 * @throws MessageException
	 * @description method for processing the game end request
	 * 
	 */
	private void processGameEnd() throws MessageException {

		state = DFAState.RDY_FOR_GAME;
		EndGameResponseMessage msgLocal = new EndGameResponseMessage();
		outBoundMessage = msgLocal;
		msgLocal.setDataField(msgLocal.POSITIVE);
		msgLocal.setHangmanStatusField(game.guesses() + "-" + game.guesses());
		msgLocal.setSolutionStatusField(game.correctPhrase());

	}

	/**
	 * @description method for disconnecting the connection
	 * 
	 */
	private void disconnect() {

		// close the session, release the session, and decrease the
		// active session count
		state = DFAState.CLOSED;

		outBoundMessage = new DisconnectResponseMessage();

		if (getNumberOfSessions() > 0)
			decrementNumberOfSessions();

	}

	/**
	 * @descrption increments the number of sessions
	 */
	public static final synchronized void incrementNumberOfSessions() {

		numberOfSessions++;

	}

	/**
	 * @descrption decrements the number of sessions
	 */
	public static final synchronized void decrementNumberOfSessions() {
		numberOfSessions--;
	}

	/**
	 * @description gets the number of sessions
	 * @return
	 */
	public static final synchronized int getNumberOfSessions() {

		return numberOfSessions;
	}

	// inner class to check if message is out of order

	public static final class MessageValidator {

		/**
		 * @param command
		 * @param state
		 * @return rtnVal
		 * @description checks if message is out of order;
		 */
		public static final boolean isMessageOutOfOrder(String command,
				int state) {

			boolean rtnVal = false;

			switch (state) {

			case (DFAState.CLOSED):
				if (command != MessageCommand.CONN
						&& command != MessageCommand.DISC)
					rtnVal = true;
				break;

			case (DFAState.RDY_FOR_GAME):
				if (command != MessageCommand.GREQ
						&& command != MessageCommand.DISC)
					rtnVal = true;
				break;

			case (DFAState.RDY_TO_PLAY):
				if (command != MessageCommand.LGUE
						&& command != MessageCommand.SGUE
						&& command != MessageCommand.ENDG
						&& command != MessageCommand.DISC)
					rtnVal = true;
				break;

			case (DFAState.LTR_GUESSED):
				if (command != MessageCommand.DISC
						&& command != MessageCommand.ENDG)
					rtnVal = true;

			default:
				rtnVal = false;
				break;
			}

			return rtnVal;
		}
	}
}
