package edu.drexel.cs544.server;

import java.io.IOException;

import edu.drexel.cs544.game.Versions;


/**
 * The entry point for the server application
 * 
 */
public class Server {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String server = null;
		
		if (args.length == 0)
			//Use default name
			server = "HangmanBot";
		else if (args.length == 1)
			server = args[0];
		else {
			System.out.println("Usage: serverName");
			System.exit(42);
		}
		
		System.out.println("Running Hangman: " + Versions.HANGMAN_VERSION);
		System.out.println("Using twitter account: " + server);
		Thread networkServiceThread = null;

		try {
		
			networkServiceThread = new Thread (new NetworkService(2, server));
			networkServiceThread.start();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			networkServiceThread = null;
			
		}
		

	}

}
