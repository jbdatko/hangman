package edu.drexel.cs544.server;

import static org.junit.Assert.*;

import java.lang.reflect.Method;

import org.junit.Test;

public class SessionTest {

	@Test
	public void testOutOfSequence() {

		Session session = new Session();
		String msg = session.processSessionRequest("HM/1.0/001/CONN//");
		System.out.println(msg);
		assertEquals(null, "HM/1.0/001/ERRO/200-Message Out of Sequence//", msg);
	}

	@Test
	public void testOutOfOrder() {

		Session session = new Session();
		// session.setTestVariables(1, MessageCommand.LGUE,1,' ', "");
		String msg = session.processSessionRequest("HM/1.0/000/LGUE/A//");
		System.out.println(msg);
		assertEquals(null, "HM/1.0/001/ERRO/300-Message Out of Order//", msg);
	}

	@Test
	public void testConnectCommand() {

		Session session = new Session();
		String msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println(msg);
		assertEquals(null, "HM/1.0/001/CRES/POS//", msg);
	}

	@Test
	public void testConnectCommandFail() {

		Session session = new Session();
		session.setTestVariables(0, "", 10, ' ', "");
		String msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println(msg);
		assertEquals(null, "HM/1.0/001/CRES/NEG//", msg);
	}

	@Test
	public void testGameRequest() {

		String msg = "";

		Session session = new Session();

		msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println("Test Game Request: " + msg);
		msg = session.processSessionRequest("HM/1.0/002/GREQ/NEW//");
		System.out.println("Test Game Request: " + msg);
		assertTrue(msg.contains("HM/1.0/002/GRES/"));
	}

	@Deprecated
	public void testGameRequestFail() {

		String msg = "";

		Session session = new Session();
		session.setTestVariables(1, MessageCommand.CONN, 1, ' ', "");
		msg = session.processSessionRequest("");
		session.setTestVariables(3, MessageCommand.GREQ, 1, ' ', "");
		msg = session.processSessionRequest("");

		assertEquals(null, "RDY_FOR_GUESS", msg);
	}

	@Test
	public void testProcessLetterGuess() {

		String msg = "";

		Session session = new Session();
		msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/002/GREQ/NEW//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/004/LGUE/A//");
		System.out.println("Process Letter Guess: " + msg);
		assertTrue(msg.contains("HM/1.0/005/LRES/"));
	}

	@Test
	public void testProcessPhraseGuess() {

		String msg = "";

		Session session = new Session();
		msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println("Process Phrase Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/002/GREQ/NEW//");
		System.out.println("Process Phrase Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/004/SGUE/HELLO//");
		System.out.println("Process Phrase Guess: " + msg);
		assertTrue(msg.contains("HM/1.0/005/SRES/"));
	}

	@Test
	public void testLetterGuessGameOver() {

		String msg = "";

		Session session = new Session();
		msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/002/GREQ/NEW//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/004/LGUE/Y//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/006/LGUE/Z//");
		System.out.println("Process Letter Guess: "+msg);

		assertTrue(msg.contains("GAME_OVER"));
	}

	@Test
	public void testPhraseGuessGameOver() {

		String msg = "";

		Session session = new Session();
		msg = session.processSessionRequest("HM/1.0/000/CONN//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/002/GREQ/NEW//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/004/SGUE/HELLO//");
		System.out.println("Process Letter Guess: " + msg);
		msg = session.processSessionRequest("HM/1.0/006/SGUE/HELLO//");
		System.out.println("Process Letter Guess: "+msg);
		
		assertTrue(msg.contains("GAME_OVER"));
	}

	@Test
	public void testEndGame() {

		String msg = "";

		Session session = new Session();
		msg = session.processSessionRequest("HM/1.0/000/CONN//");
		msg = session.processSessionRequest("HM/1.0/002/GREQ/NEW//");
		msg = session.processSessionRequest("HM/1.0/004/ENDG//");
		System.out.println(msg);

		assertTrue(msg.contains("ERES"));
	}
	
	@Test
	public void testMalformedMessage(){
		
		Session session = new Session();
		String msg = session.processSessionRequest("HM/1.0/000///");
		System.out.println(msg);
		
		
	}
	
	@Test
	public void testSequence(){
		
		Session session = new Session();
		System.out.println(session.processSessionRequest("HM/1.0/000/CONN//"));
		System.out.println(session.processSessionRequest("HM/1.0/002/GREQ/NEW//"));
		System.out.println(session.processSessionRequest("HM/1.0/004/LGUE/A//"));
		System.out.println(session.processSessionRequest("HM/1.0/006/SGUE/HELLO//"));
		System.out.println(session.processSessionRequest("HM/1.0/008/SGUE/HELLO//"));
		System.out.println(session.processSessionRequest("HM/1.0/010/GREQ/NEW//"));
		System.out.println(session.processSessionRequest("HM/1.0/012/ENDG//"));
		System.out.println(session.processSessionRequest("HM/1.0/014/DISC//"));
	}
}
