package edu.drexel.cs544.server;

import java.io.IOException;
import java.net.Socket;

import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;

public class ServerProcess implements Runnable {

	private Socket socket;
	private Session session;
	private TwitterInputStream in;
	private TwitterOutputStream out;

	/**
	 * 
	 * @param socket
	 * @throws IOException
	 * @description Constructor that sets the socket for listening purposes once
	 *              the connection has been established.
	 * 
	 */

	public ServerProcess(Socket socket) {

		this.socket = socket;
		this.session = new Session();

		try {
			this.in = (TwitterInputStream) socket.getInputStream();
			this.out = (TwitterOutputStream) socket.getOutputStream();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("Error in getting streams.");
		}
	}

	/**
	 * 
	 * @description listens for messages, sends messages, executes DFA
	 * 
	 */

	public void run() {

		// process the session messages
		do {

			try {

				// read message, execute DFA, write message back to the server
				writeMessage(session.processSessionRequest(readMessage()));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Error in processing the message.");
			

			}
		} while ((session.getState() != DFAState.CLOSED));

		// close the socket
		try {
			in = null;
			out = null;
			socket.close();
			socket = null;
			session = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error closing the socket.");
		}

	}

	/**
	 * @return
	 * @throws IOException
	 * @description reads the inbound messages from the server socket
	 * 
	 */

	public String readMessage() throws IOException {

		// Block forever until a message is received
		return in.read_wait();

	}

	/**
	 * @param msg
	 * @throws IOException
	 * @description writes message back out to the server socket
	 * 
	 */

	public void writeMessage(String msg) throws IOException {

		// Just echo it back for now
		out.write(msg);

	}

}
