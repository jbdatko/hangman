package edu.drexel.cs544.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.drexel.cs544.net.TwitterServerSocketFactory;

/**
 * @author arindambiswas
 * 
 */

public class NetworkService implements Runnable {

	private final ExecutorService connectionManager;
	private final ServerSocket socket;
	private boolean shutDown = false;

	/**
	 * @param numberOfConnections
	 * @throws IOException
	 * @description Constructor that creates the executor service to manage the
	 *              connection pool and creates the connection socket.
	 * 
	 */

	public NetworkService(int numberOfConnections, String serverName) throws IOException {
		if (null == serverName)
			throw new NullPointerException("Server name can't be null");

		// executor service that manages the connection pool
		connectionManager = Executors.newFixedThreadPool(numberOfConnections);
		// socket for listening for new connection requests
		socket = TwitterServerSocketFactory.getInstance(serverName).createServerSocket();
	}

	/**
	 * @description Continuously listens for connection requests until the
	 *              program is terminated.
	 * 
	 */

	public void run() {
		// TODO Auto-generated method stub

		// start listening for connection requests
		try {
			// if there is a new connection request, accept the connection and
			// spawn a new thread to process the requests for that specific
			// connection
			while (!shutDown) {
			
				connectionManager.execute(new ServerProcess(socket.accept()));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			// shutdown running threads and end the connection service
			connectionManager.shutdown();

		}
	}

}