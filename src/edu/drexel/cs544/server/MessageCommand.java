package edu.drexel.cs544.server;

public interface MessageCommand {

		public static final String CONN = "CONN";
		
		public static final String GREQ = "GREQ";
		
		public static final String DISC = "DISC";
		
		public static final String LGUE = "LGUE";
		
		public static final String SGUE = "SGUE";
		
		public static final String ENDG = "ENDG";
	
}
