package edu.drexel.cs544.server.game.test;

import java.util.*;

import edu.drexel.cs544.server.game.Game;
import edu.drexel.cs544.server.game.Guess;
import edu.drexel.cs544.server.game.GameState;

public class UnitTestClient 
{
	private static Scanner scan;

	private static char charGuess;
	private static char[] buffer;
	
	private static String wordGuess;
	
	private static String fileName;
	
	private static int selection;
	
	private static Game theGame;
	
	private static boolean playing;
	
	private static Guess guess;
	
	public static void main(String args[]) throws Exception
	{
		scan = new Scanner(System.in);
		playing = true;
		
		System.out.println("What file contains the word list?");
		
		fileName = scan.next();
		
		theGame = new Game(fileName, 6);
		
		while(playing)
		{
		
		System.out.println("What do you want to do: ");
		System.out.println("1) Guess letter");
		System.out.println("2) Guess word");
		System.out.println("3) see word status");
		System.out.println("4) See guesses");
		System.out.println("5) Quit.");
		
		selection = scan.nextInt();
		
			switch(selection)
			{
			case 1:
					System.out.println("What Letter?");
					
					wordGuess = scan.next();
					
					if(wordGuess.length() > 1)
					{
						System.out.println("Not a letter!.");
						break;
					}
					
					buffer = wordGuess.toCharArray();
					
					charGuess = buffer[0];
					
					guess = new Guess(charGuess);
					
					theGame.processGuess(guess);
					break;
			case 2:
					System.out.println("What word?");
					
					wordGuess = scan.next();
					
					guess = new Guess(wordGuess);
					
					theGame.processGuess(guess);
					
					break;
			case 3:
					System.out.print("Puzzle Status: ");
					System.out.println(theGame.getPuzzleStatus());
					break;
					
			case 4:
					System.out.println("UsedGuesses-TotalAllowedGuesses");
					System.out.println(theGame.usedGuesses() + "-" + theGame.guesses());
					break;
					
			case 5:
					System.out.println("QUIT");
					System.exit(1);
					
			case 6:
					System.out.println("Please enter a real number, please.");
					break;
			default:
					break;
			}
			
			if(selection == 1 || selection == 2)
			{
				if(guess.getResult())
				{
					System.out.println("Your guess was correct!");
				}
				else
				{
					System.out.println("Your guess was incorrect.");
				}
			}
			
			if(theGame.getState() == GameState.WON)
			{
				System.out.println("You won!");
				System.out.println("The word was: " + theGame.correctPhrase());
				System.out.println("Thanks for playing!");
				theGame.startGame(null);
			}
			
			else if(theGame.getState() == GameState.LOST)
			{
				System.out.println("So you get NOTHING! You LOSE!  GOOD DAY SIR!");
				System.out.println("The word was: " + theGame.correctPhrase());
				System.out.println("YOU only got: " + theGame.getPuzzleStatus());
				System.out.println("Play again if you dare.");
				theGame.startGame(null);
			}
			
			else if(theGame.getState() == GameState.PLAYING)
			{
				System.out.println(theGame.getPuzzleStatus() + " : " + theGame.usedGuesses() + "-" + theGame.guesses());
			}
		}	
	}
}
