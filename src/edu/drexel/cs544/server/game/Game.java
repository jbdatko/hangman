package edu.drexel.cs544.server.game;

import java.io.*;
import java.util.*;

import edu.drexel.cs544.game.HangmanGame;


public class Game implements HangmanGame{
	
	private static final char NULL = '\u0000';
	
	private String thePhrase;
	private String phraseStatus;
	
	private char[] buffer;
	
	private int guesses;
	private int usedGuesses;
	
	private Scanner fileReader;
	
	private File file;
	
	private ArrayList<String> wordList;
	
	private GameState state;
	
	public Game(String fileName, int numGuesses) 
	{
		
		try
		{
			file = new File(fileName);
		
			fileReader = new Scanner(file);
			
			wordList = constructList(fileReader);
		}
		
		catch(FileNotFoundException e)
		{
			//System.err.println("FileNotFoundException: " + e.getMessage());
			
			System.out.println("Using default word list!");
			
			wordList = constructDefaultList();
		}
		
		finally
		{
			this.init(numGuesses);	
		}
	}
	
	private void init(int numGuesses){
		
		guesses = numGuesses;

		try {
			thePhrase = pickRandom(wordList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// debugging
		// System.out.println(thePhrase);

		phraseStatus = hidePhrase(thePhrase);

		state = GameState.PLAYING;

		usedGuesses = 0;
		
	}
	
	public ArrayList<String> constructDefaultList()
	{
		ArrayList<String> temp = new ArrayList<String>();
		
		temp.add("HELLO");
		temp.add("DEFAULT");
		temp.add("BATMAN");
		temp.add("FUTURAMA");
		temp.add("BENDER");
		
		return temp;
	}
	
	public ArrayList<String> constructList(Scanner scan)
	{
		ArrayList<String> tempList = new ArrayList<String>();
		
		
		
		while(scan.hasNext())
		{
			tempList.add(scan.next());
		}
		
		return tempList;
			
	}
	
	public String pickRandom(ArrayList<String> wordList) throws Exception
	{
		
		String temp;
		
		Random randomizer = new Random(System.currentTimeMillis());
		
		try
		{
			
			temp = wordList.get(randomizer.nextInt(wordList.size())).toUpperCase();
		}
		
		catch(IllegalArgumentException e)
		{
			System.err.println("IllegalArgumentException: " + e.getMessage());
			
			System.out.println("File empty!  Using default wordlist.");
			ArrayList<String> tempList = constructDefaultList();
			temp = tempList.get(randomizer.nextInt(tempList.size())).toUpperCase();
		}
		
		return temp;
	}
	
	@Override
	public void processGuess(Guess theGuess)
	{
		if(state == GameState.PLAYING)
		{
			char tempChar;
			
			if(theGuess.getString() != null)
			{
				if(thePhrase.equalsIgnoreCase(theGuess.getString())) 
				{
					phraseStatus = theGuess.getString().toUpperCase();
					theGuess.setResult(true);
				}
				else
				{
					usedGuesses++;
					theGuess.setResult(false);
				}
			}
			
			else if(theGuess.getChar() != NULL)
			{
				tempChar = theGuess.getChar();
				
				tempChar = Character.toUpperCase(tempChar);
				
				if(thePhrase.indexOf(tempChar) >= 0)
				{
					updatePuzzle(tempChar);
					theGuess.setResult(true);
				}
				else
				{
					usedGuesses++;
					theGuess.setResult(false);
				}
			}
		}
		
		if(usedGuesses >= guesses)
		{
			state = GameState.LOST;
		}
		else if( thePhrase.equalsIgnoreCase(phraseStatus) )
		{
			state = GameState.WON;
		}
		
	}

	public String hidePhrase(String thePhrase)
	{
		String encryptedPhrase = "";
		
		
		for(int i = 0; i < thePhrase.length(); i++)
		{
			encryptedPhrase += "-";
		}
		
		return encryptedPhrase;
	}
	
	public void updatePuzzle(char guess)
	{
		buffer = phraseStatus.toCharArray();
		
		for(int i = 0; i < thePhrase.length(); i++)
		{
			if(thePhrase.charAt(i) == guess)
			{
				buffer[i] = guess;
			}
		}
		
		phraseStatus = new String(buffer);
	}
	
	@Override
	public String getPuzzleStatus()
	{
		return phraseStatus;
	}
	
	@Override
	public int guesses()
	{
		return guesses;
	}
	
	@Override
	public int usedGuesses()
	{
		return usedGuesses;
	}
	
	@Override
	public String correctPhrase()
	{
		return thePhrase;
	}
	
	@Override
	public GameState getState()
	{
		return state;
	}

	@Override
	public boolean startGame(String gameServer) {
		
		this.init(guesses);
		
		return true;
		
	}

	@Override
	public void endGame() {
		//A no-op for the server
		
	}
}
