package edu.drexel.cs544.server.game;

public class Guess {
	
	private static final char NULL = '\u0000';
	
	private char guessChar;
	private String guessString;
	private boolean guessResult;
	
	public Guess(char theChar)
	{
		guessChar = theChar;
		guessString = null;
		guessResult = false;
	}
	
	public Guess(String theString)
	{
		guessString = theString;
		guessChar = NULL;
		guessResult = false;
	}
	
	public char getChar()
	{
		return guessChar;
	}
	
	public String getString()
	{
		return guessString;
	}
	
	public boolean getResult()
	{
		return guessResult;
	}
	
	public void setResult(boolean correctness)
	{
		guessResult = correctness;
	}
}