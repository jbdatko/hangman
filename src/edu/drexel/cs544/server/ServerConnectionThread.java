package edu.drexel.cs544.server;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;

public class ServerConnectionThread implements Runnable {

	protected Socket soc;

	public ServerConnectionThread(Socket s) {
		this.soc = s;
		new Thread(this).start();

	}

	public void run() {

		try {

			// Main loop of the session

			// At the moment, this is just an ECHO server. It will wait for a
			// message from the connected socket and echo it back
			while (true) {
				// Grab the IO objects from the socket
				TwitterInputStream in = (TwitterInputStream) soc
						.getInputStream();
				TwitterOutputStream out = (TwitterOutputStream) soc
						.getOutputStream();

				// Block forever until a message is received
				String msgRec = in.read_wait();

				// Just echo it back for now
				out.write(msgRec);

			}

		} catch (SocketException e) {
			// Socket was closed
			// This ok... for now just print the trace.
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				soc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} // end finally

	}

}
