package edu.drexel.cs544.client;

import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.game.Versions;
import edu.drexel.cs544.net.util.TwitterAccountRepository;
import edu.drexel.cs544.server.game.GameState;
import edu.drexel.cs544.server.game.Guess;

/**
 * 
 * Client Bot to play hangman
 * @author Josh Datko
 *
 */
public class ClientBot {
	
	/**
	 * The game interface
	 */
	private HangmanGame game;
	/**
	 * Set of used guesses to prevent duplicates
	 */
	private Set<String> used; 
	/**
	 * Random number generator
	 */
	private Random r = null;
	/**
	 * Date instance for Random and time stamps
	 */
	private Date d = new Date();
	/**
	 * Time to sleep between guesses
	 */
	private static final long SLEEP_TIME = 10000; // 10 seconds
	
	private static String SERVER_NAME;
	
	/**
	 * Create the client hangman playing bot
	 * @param g the game interface
	 */
	public ClientBot(HangmanGame g){
		this.game = g;
		
		r = new Random(d.getTime());
	}
	
	/**
	 * Play the game until finished
	 * @throws Exception if there is a connection problem
	 */
	public void play() throws Exception{
		
		this.used = new HashSet<String>();
		
		System.out.println("Connecting to : " + SERVER_NAME);
		
		if (!this.game.startGame(SERVER_NAME))
			throw new Exception("Game failed to start");
		
		System.out.println("Starting Game at " + d.toString());
	
		while(GameState.PLAYING == this.game.getState()){
			String guess = this.getGuess();
			System.out.println("Guessing: " + guess);
			this.game.processGuess(new Guess(guess.charAt(0)));
			System.out.println("Bot's game status: "
				+ this.game.getPuzzleStatus() + " : "
				+ this.game.usedGuesses() + "-" + this.game.guesses());
			
			System.out.println("Waiting " + SLEEP_TIME + " ms");
			Thread.sleep(SLEEP_TIME);
		}
		
		if (GameState.WON == this.game.getState()){
			System.out.println("Congrats, the bot won!");
			
		} else {
			System.out.println("Sorry, the bot lost!");
		}
		
		
		System.out.println("The word was: " + this.game.correctPhrase());
		
		System.out.println("Ending Game at " + d.toString());
		
		this.game.endGame();
	}
	
	/**
	 * Get the next random, unused character to guess
	 * @return a random guess
	 */
	private String getGuess(){
		
		String guess = new String();
		guess = guess + (char)(r.nextInt(26) + 'a');
		
		while (!this.used.add(guess)){
			guess = "";
			guess = guess + (char)(r.nextInt(26) + 'a');
		}
		
		return guess;
		

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			//Sleep for discovery
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			
		}

		String client = null;
		String server = null; 
		if (args.length == 0) {
			client = "Galgenmann";
			server = "HangmanBot";
		} else if (args.length == 2){
			client = args[0];
			server = args[1];
		} else {
			System.out.println("Usage: clientName serverName");
			System.exit(42);
		}
			
		TwitterAccountRepository repo = new TwitterAccountRepository();
		if (!repo.contains(client)){
			System.out.println("client is invalid: " + client);
			System.exit(42);
		}
		
		SERVER_NAME = server;
		
		System.out.println("Running client bot version: " + Versions.HANGMAN_VERSION);
		System.out.println("Client: " + client + " Server: " + server);
		
		HangmanGame game = new ClientController(client);
		
		ClientBot bot = new ClientBot(game);
		
		try {
			while(true){
				bot.play();
			}
		} catch (Exception e) {
			System.out.println("ClientBot died");
			e.printStackTrace();
		}
		
		System.exit(0);
		
		

	}


}
