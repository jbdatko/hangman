package edu.drexel.cs544.client;

import edu.drexel.cs544.gui.HangmanGUI;
import edu.drexel.cs544.net.util.TwitterAccountRepository;

/**
 * The entry point for the client application
 *
 */
public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String account = null;
		
		if ( args.length > 0 ) {
			TwitterAccountRepository repo = new TwitterAccountRepository();
			if (repo.contains(args[0]))
				account = args[0];
		}
		if (null == account)
			account = "gallowsDude";
		
		//Create the controller
		ClientController controller = new ClientController(account);
		
		//Pass the controller to the GUI, which will spawn a new thread
		new HangmanGUI(controller);

	}

}
