package edu.drexel.cs544.client;

public interface InboundMessageKeyword {
	
	public static final String CRES = "CRES";
	
	public static final String GRES = "GRES";
	
	public static final String LRES = "LRES";
	
	public static final String SRES = "SRES";
	
	public static final String ERES = "ERES";
	
	public static final String DRES = "DRES";
	
	public static final String ERRO = "ERRO";

}
