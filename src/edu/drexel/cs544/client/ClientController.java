package edu.drexel.cs544.client;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

import javax.net.SocketFactory;
import edu.drexel.cs544.game.HangmanGame;
import edu.drexel.cs544.net.TwitterInputStream;
import edu.drexel.cs544.net.TwitterOutputStream;
import edu.drexel.cs544.net.TwitterSocketFactory;
import edu.drexel.cs544.server.DFAState;
import edu.drexel.cs544.server.game.GameState;
import edu.drexel.cs544.server.game.Guess;
import edu.drexel.cs544.util.*;

public class ClientController implements HangmanGame {

	private static final char NULL = '\u0000';
	private static final int  SEQUENCE_LIMIT = 1000;
	private static final int  MALFORMED_MESSAGE_RETRIES = 5;

	/**
	 * Socket factory for the client
	 */
	private SocketFactory fac;

	/**
	 * Re-usable socket instance
	 */
	private Socket soc = null;

	/**
	 * Input and output streams
	 */
	private TwitterInputStream in;
	private TwitterOutputStream out;

	/**
	 * Protocol version
	 */
	private final double version;

	/**
	 * Sequence
	 */
	private int sequence;

	/**
	 * Game state
	 */
	private GameState state;

	/**
	 * Game play variables
	 */
	private String correctPhrase;
	private String puzzleStatus;
	private int guesses;
	private int usedGuesses;

	/**
	 * DFA State
	 */
	private int dfaState;
	
	/**
	 * Do not send messages flag
	 */
	private boolean receiveOnly;
	
	/**
	 * Malformed error message received flag 
	 */
	private boolean malformedErrorMessageReceived;
	
	/**
	 * Number of consecutive outgoing malformed messages
	 */
	private int outgoingMalformedMessageCount;
	
	/**
	 * Number of consecutive incoming malformed messages
	 */
	private int incomingMalformedMessageCount;
	
	/**
	 * Indicates a forced disconnect
	 */
	private boolean forcedDisconnect;

	/**
	 * Constructor
	 */
	public ClientController(String account) {
		
		if (null == account)
			this.fac = TwitterSocketFactory.getInstance();
		else
			this.fac = TwitterSocketFactory.getInstance(account);
		
		// Default to version 1.0
	    version = 1.0;
				
		resetVariables();		
	}
	
	/**
	 * Constructor
	 */
	public ClientController(double version) {
		
		// Set the version
	    this.version = version;
				
		resetVariables();		
	}
	
	/**
	 * Resets the game variables
	 */
	private void resetVariables() {
		
		// Reset sequence
		sequence = 0;
		
		// Set Game State to null
		state = null;
		
		// Game variables
		correctPhrase = "";
		puzzleStatus = "";
		this.setGuesses(0);
		this.setUsedGuesses(0);


		// Set DFA State to CLOSED
		dfaState = DFAState.CLOSED;
		
		// Reset error variables
		resetErrorVariables();
	}
	
	/**
	 * Resets all of the error related variables
	 */
	private void resetErrorVariables() {
		
		receiveOnly = false;
		malformedErrorMessageReceived = false;
		outgoingMalformedMessageCount = 0;
		incomingMalformedMessageCount = 0;
		forcedDisconnect = false;
	}

	@Override
	public String getPuzzleStatus() {

		return puzzleStatus;
	}

	@Override
	public int usedGuesses() {

		return usedGuesses;
	}
	
	private void setUsedGuesses(int g){
		usedGuesses = g;
	}

	@Override
	public int guesses() {

		return guesses;
	}
	
	private void setGuesses(int g){
		guesses = g;
	}

	@Override
	public GameState getState() {

		return state;
	}

	@Override
	public String correctPhrase() {

		return correctPhrase;
	}

	@Override
	public boolean startGame(String gameServer) 
			throws SocketException {

		boolean gameStarted = false;

		if (null == this.soc) {
			try {
				// Create a socket and attempt to connect
				this.soc = fac.createSocket(gameServer, 0);
			} catch (Exception e) {

				e.printStackTrace();
				return false;
			}
		} else if (this.soc.isConnected()) {

			TwitterInputStream inputStream = null;

			try {
				inputStream = (TwitterInputStream) this.soc
						.getInputStream();
			} catch (IOException e) {

				e.printStackTrace();
				return false;
			}

			if (!inputStream.getRemoteEndpoint().equalsIgnoreCase(gameServer)) {
				
				try {
					this.soc.close();
					this.soc = this.fac.createSocket(gameServer, 0);
				} catch (IOException e) {

					e.printStackTrace();
					return false;
				}
			}
		} else if (this.soc.isClosed()) {
			try {
				this.soc = this.fac.createSocket(gameServer, 0);
			} catch (Exception e) {

				e.printStackTrace();
				return false;
			}
		}

		try {
			this.in = (TwitterInputStream) this.soc.getInputStream();
			this.out = (TwitterOutputStream) this.soc.getOutputStream();
			
			// Reset variables
			resetVariables();
			
			// Process the connect
			if (processConnect()) {
				
				// Process the game request
				gameStarted = processGameRequest();
			}
			
		} catch (SocketException e) {
			
			throw e;
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		return gameStarted;
	}

	/**
	 * Establishes a connection
	 * 
	 * @return connection result
	 * @throws Exception
	 */
	private boolean processConnect() 
			throws SocketException {

		boolean result = false;

		try {
			// Send Connect Message
			String out_msg_string = HangmanMessageFactory
					.encode(new ConnectMessage(this.version, this.sequence));
			writeMessage(out_msg_string);
			
		} catch (Exception e) {

			throw new SocketException(e.getMessage());
		}
		
		// Transition DFA state
		dfaState = DFAState.CONN_INIT;
		
		try {
			// Process Connect Response Message
			result = processConnectResponse(readMessage());
			
		} catch (MessageException e) {
			
			if(malformedErrorMessageReceived == true) {
				
				malformedErrorMessageReceived = false;
				result = processConnect();	
				
			} else {
			
				++incomingMalformedMessageCount;
				
				if(incomingMalformedMessageCount < MALFORMED_MESSAGE_RETRIES) {
					
					// Send error message
					sendErrorMessage("100-Malformed Message");
					
					// Try again but do not re-send the original message
					receiveOnly = true;
					result = processConnect();	
					
				} else {
					
					throw new SocketException(e.getMessage());
				}
			}
		} catch (Exception e) {
			
			throw new SocketException(e.getMessage());
		}
		
		// Reset error variables
		resetErrorVariables();

		return result;
	}

	/**
	 * 
	 * @param resp_string
	 * @return
	 */
	private boolean processConnectResponse(String resp_string) 
			throws SocketException, MessageException {

		boolean result = false;

		try {

			Message resp_obj = HangmanMessageFactory.decode(resp_string);

			if (!isMessageOutOfSequence(Integer.parseInt(resp_obj.getSequence())) && 
					!isMessageOutOfOrder(resp_obj.getMessageType().getKeyword())) {
				
				if(!resp_obj.getMessageType().getKeyword().equals(InboundMessageKeyword.ERRO)) {

					if (resp_obj.getDataField().equals("POS")) {
						
						dfaState = DFAState.RDY_FOR_GAME;
						result = true;
						
					} else {
						
						dfaState = DFAState.CLOSED;
					}
				} else {
					
					processErrorMessage(resp_obj.getDataField());
					
					if(forcedDisconnect == true)
						throw new SocketException("Forced disconnection!");
					else
						throw new MessageException("Malformed error message received!");
				}
			} else {
				
				// Forced disconnect
				forcedDisconnect = true;
				
				// Disconnect
				endGame();
				
				// Throw exception to let the UI know that we disconnected
				throw new SocketException("Forced disconnection!");
			}
		} catch (MessageException e) {

			throw e;
		}

		return result;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean processGameRequest() 
			throws SocketException {

		boolean result = false;

		try {
			// Send Game Request Message
			String out_msg_string = HangmanMessageFactory
					.encode(new GameRequestMessage(this.version, this.sequence, "NEW"));
			writeMessage(out_msg_string);
		
		} catch (Exception e) {

			throw new SocketException(e.getMessage());
		}

		// Transition DFA state
		dfaState = DFAState.GAME_INIT;
		
		try {
			// Process Game Request Response Message
			result = processGameRequestResponse(readMessage());
			
		} catch (MessageException e) {
			
			if(malformedErrorMessageReceived == true) {
				
				malformedErrorMessageReceived = false;
				result = processGameRequest();
				
			} else {

				++incomingMalformedMessageCount;
	
				if(incomingMalformedMessageCount < MALFORMED_MESSAGE_RETRIES) {
	
					// Send error message
					sendErrorMessage("100-Malformed Message");
					
					// Try again but do not re-send the original message
					receiveOnly = true;
					result = processGameRequest();
	
				} else {
	
					throw new SocketException(e.getMessage());
				}
			}
		} catch (Exception e) {

			throw new SocketException(e.getMessage());
		}
		
		// Reset error variables
		resetErrorVariables();

		return result;
	}

	/**
	 * 
	 * @param resp_string
	 * @return
	 * @throws SocketException
	 * @throws MessageException
	 */
	private boolean processGameRequestResponse(String resp_string)
			throws SocketException, MessageException {
		
		boolean result = false;

		try {

			Message resp_obj = HangmanMessageFactory.decode(resp_string);

			if (!isMessageOutOfSequence(Integer.parseInt(resp_obj.getSequence())) &&
					!isMessageOutOfOrder(resp_obj.getMessageType().getKeyword())) {
				
				if(!resp_obj.getMessageType().getKeyword().equals(InboundMessageKeyword.ERRO)) {

					if (resp_obj.getDataField().equals("POS")) {
	
						this.setUsedGuesses( Integer.parseInt(resp_obj.getHangmanStatusField().substring(0, 1)));
						this.setGuesses( Integer.parseInt(resp_obj.getHangmanStatusField().substring(2, 3)) );
						puzzleStatus = resp_obj.getSolutionStatusField();
						correctPhrase = null;
	
						// Transition game state and DFA state
						state = GameState.PLAYING;
						dfaState = DFAState.RDY_TO_PLAY;
						
						result = true;
						
					} else {
						// Transition DFA state
						dfaState = DFAState.RDY_FOR_GAME;
					}
				} else {
					
					processErrorMessage(resp_obj.getDataField());
					
					if(forcedDisconnect == true)
						throw new SocketException("Forced disconnection!");
					else
						throw new MessageException("Malformed error message received!");
				}
			} else {
				
				// Forced disconnect
				forcedDisconnect = true;
				
				// Disconnect
				endGame();
				
				// Throw exception to let the UI know that we disconnected
				throw new SocketException("Forced disconnection!");
			}
		} catch (MessageException e) {

			throw e;
		}
		
		return result;
	}

	@Override
	public void processGuess(Guess theGuess) 
			throws SocketException {

		if (state == GameState.PLAYING) {

			if (theGuess.getString() != null) {
				
				try {
					// Process the solution guess
					processSolutionGuess(theGuess);
					
				} catch (SocketException e) {
					
					throw e;
				}
			} else if (theGuess.getChar() != NULL) {
				
				try {
					// Process the letter guess
					processLetterGuess(theGuess);
					
				} catch (SocketException e) {
					
					throw e;
				}
			}
		}
	}

	/**
	 * 
	 * @param theGuess
	 * @throws SocketException
	 */
	private void processSolutionGuess(Guess theGuess) 
			throws SocketException {
		
		try {
			// Send Solution Guess Message
			String out_msg_string = HangmanMessageFactory
					.encode(new SolutionGuessMessage(this.version,
							this.sequence, theGuess.getString()));
			writeMessage(out_msg_string);
		
		} catch (Exception e) {
	
			throw new SocketException(e.getMessage());
		}

		// Transition DFA state
		dfaState = DFAState.SOL_GUESSED;
	
		try {
			// Process Solution Guess Response Message
			theGuess.setResult(processGuessResponse(readMessage()));
			
		} catch (MessageException e) {
			
			if(malformedErrorMessageReceived == true) {
				
				malformedErrorMessageReceived = false;
				processSolutionGuess(theGuess);
				
			} else {
	
				++incomingMalformedMessageCount;
		
				if(incomingMalformedMessageCount < MALFORMED_MESSAGE_RETRIES) {
		
					// Send error message
					sendErrorMessage("100-Malformed Message");
					
					// Try again but do not re-send the original message
					receiveOnly = true;
					processSolutionGuess(theGuess);
		
				} else {
		
					throw new SocketException(e.getMessage());
				}
			}
		} catch (Exception e) {
	
			throw new SocketException(e.getMessage());
		}
		
		// Reset error variables
		resetErrorVariables();
	}
	
	/**
	 * 
	 * @param theGuess
	 * @return
	 * @throws SocketException
	 */
	private void processLetterGuess(Guess theGuess)
			throws SocketException {
		
		try {
			char letter = Character.toUpperCase(theGuess.getChar());

			// Send Letter Guess Message
			String out_msg_string = HangmanMessageFactory
					.encode(new LetterGuessMessage(this.version,
							this.sequence, Character.toString(letter)));
			writeMessage(out_msg_string);
		
		} catch (Exception e) {
	
			throw new SocketException(e.getMessage());
		}

		// Transition DFA state
		dfaState = DFAState.LTR_GUESSED;
	
		try {
			// Process Solution Guess Response Message
			theGuess.setResult(processGuessResponse(readMessage()));
			
		} catch (MessageException e) {
			
			if(malformedErrorMessageReceived == true) {
				
				malformedErrorMessageReceived = false;
				processLetterGuess(theGuess);
				
			} else {
	
				++incomingMalformedMessageCount;
		
				if(incomingMalformedMessageCount < MALFORMED_MESSAGE_RETRIES) {
		
					// Send error message
					sendErrorMessage("100-Malformed Message");
					
					// Try again but do not re-send the original message
					receiveOnly = true;
					processLetterGuess(theGuess);
		
				} else {
		
					throw new SocketException(e.getMessage());
				}
			}
		} catch (Exception e) {
	
			throw new SocketException(e.getMessage());
		}
		
		// Reset error variables
		resetErrorVariables();
	}

	/**
	 * 
	 * @param resp_string
	 * @return
	 * @throws Exception
	 */
	private boolean processGuessResponse(String resp_string) 
			throws SocketException, MessageException {

		boolean result = false;

		try {

			Message resp_obj = HangmanMessageFactory.decode(resp_string);

			if (!isMessageOutOfSequence(Integer.parseInt(resp_obj.getSequence())) &&
					!isMessageOutOfOrder(resp_obj.getMessageType().getKeyword())) {
				
				if(!resp_obj.getMessageType().getKeyword().equals(InboundMessageKeyword.ERRO)) {

					this.setUsedGuesses( Integer.parseInt(resp_obj.getHangmanStatusField().substring(0, 1)) );
					this.setGuesses(Integer.parseInt(resp_obj.getHangmanStatusField().substring(2, 3)));
					puzzleStatus = resp_obj.getSolutionStatusField();
	
					if (resp_obj.getDataField().equals("POS")) {
	
						// Transition DFA state
						dfaState = DFAState.RDY_TO_PLAY;
						
						result = true;
					}
	
					if (resp_obj.isGameOverFieldSet()) {
	
						correctPhrase = resp_obj.getHangmanStatusField();
	
						if (resp_obj.getGameOverField().equals("WIN"))
							state = GameState.WON;
						else
							state = GameState.LOST;
	
						// Transition DFA state
						dfaState = DFAState.RDY_FOR_GAME;
					}
				} else {
					
					processErrorMessage(resp_obj.getDataField());
					
					if(forcedDisconnect == true)
						throw new SocketException("Forced disconnection!");
					else
						throw new MessageException("Malformed error message received!");
				}
			} else {
				
				// Forced disconnect
				forcedDisconnect = true;
				
				// Disconnect
				endGame();
				
				// Throw exception to let the UI know that we disconnected
				throw new SocketException("Forced disconnection!");
			}
		} catch (MessageException e) {

			throw e;
		}

		return result;
	}

	@Override
	public void endGame() 
			throws SocketException {

		try {
			
			if (dfaState == DFAState.RDY_TO_PLAY && forcedDisconnect != true)
				processEndGame();

			// Disconnect
			if (null != this.soc)
				processDisconnect();
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		// Then once all is shutdown, close the socket
		try {
			if (null != this.soc) {
				this.soc.close();
				//set to null
				this.soc = null;
			}
			
		} catch (IOException e) {

			e.printStackTrace();
			this.soc = null;
		}
	}

	/**
	 * 
	 * @throws SocketException
	 */
	private void processEndGame() 
			throws SocketException {

		try {
			
			// Send Game End Message
			String out_msg_string = HangmanMessageFactory
					.encode(new EndGameMessage(this.version, this.sequence));
			writeMessage(out_msg_string);
			
		} catch(Exception e) {

			throw new SocketException(e.getMessage());
		}

		// Transition DFA state
		dfaState = DFAState.GAME_END_INIT;
		
		try {
			// Process End Game Response Message
			processEndGameResponse(readMessage());
			
		} catch (MessageException e) {
			
			if(malformedErrorMessageReceived == true) {
				
				malformedErrorMessageReceived = false;
				processEndGame();
				
			} else {

				++incomingMalformedMessageCount;
	
				if(incomingMalformedMessageCount < MALFORMED_MESSAGE_RETRIES) {
	
					// Send error message
					sendErrorMessage("100-Malformed Message");
					
					// Try again but do not re-send the original message
					receiveOnly = true;
					processEndGame();

				} else {
	
					throw new SocketException(e.getMessage());
				}
			}
		} catch (Exception e) {

			throw new SocketException(e.getMessage());
		}
		
		// Reset error variables
		resetErrorVariables();
	}

	/**
	 * 
	 * @param resp_string
	 * @throws Exception
	 */
	private void processEndGameResponse(String resp_string) 
			throws SocketException, MessageException {

		try {
			Message resp_obj = HangmanMessageFactory.decode(resp_string);

			if (!isMessageOutOfSequence(Integer.parseInt(resp_obj.getSequence())) &&
					!isMessageOutOfOrder(resp_obj.getMessageType().getKeyword())) {
				
				if(!resp_obj.getMessageType().getKeyword().equals(InboundMessageKeyword.ERRO)) {

					this.setUsedGuesses( Integer.parseInt(resp_obj.getHangmanStatusField().substring(0, 1)));
					this.setGuesses( Integer.parseInt(resp_obj.getHangmanStatusField().substring(2, 3)));
					puzzleStatus = resp_obj.getSolutionStatusField();
					correctPhrase = resp_obj.getSolutionStatusField();
	
					// Transition game state and DFA state
					state = null;
					dfaState = DFAState.RDY_FOR_GAME;
					
				} else {
					
					processErrorMessage(resp_obj.getDataField());
					
					if(forcedDisconnect == true)
						throw new SocketException("Forced disconnection!");
					else
						throw new MessageException("Malformed error message received!");
				}
			} else {
				
				// Forced disconnect
				forcedDisconnect = true;
				
				// Disconnect
				endGame();
				
				// Throw exception to let the UI know that we disconnected
				throw new SocketException("Forced disconnection!");
			}
		} catch (MessageException e) {

			throw e;
		}
	}

	/**
	 * 
	 * @throws SocketException
	 * @throws MessageException
	 */
	private void processDisconnect() 
			throws SocketException, MessageException {

		try {	
			// Send Disconnect Message
			String out_msg_string = HangmanMessageFactory
					.encode(new DisconnectMessage(this.version, this.sequence));
			writeMessage(out_msg_string);
			
		} catch (Exception e) {

			throw new SocketException(e.getMessage());
		}
		
		// Transition game state and DFA state
		state = null;
		dfaState = DFAState.CLOSED;

		// Do not care about response
	}
	
	/**
	 * 
	 * @param errorMessage
	 * @throws MessageException
	 */
	private void sendErrorMessage(String errorMessage) {
		
		try {
			// Send Error Message
			receiveOnly = false;
			String out_msg_string = HangmanMessageFactory
					.encode(new ErrorMessage(this.version, this.sequence, errorMessage));
			writeMessage(out_msg_string);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param errorMessage
	 */
	private void processErrorMessage(String errorMessage) {

		// Set appropriate error variables
		if(errorMessage.startsWith(ErrorCode.MALFORMED)) {
			
			++outgoingMalformedMessageCount;
			
			if(outgoingMalformedMessageCount >= MALFORMED_MESSAGE_RETRIES) {
				
				forcedDisconnect = true;
				
			} else {
				
				malformedErrorMessageReceived = true;
			}
		} else if(errorMessage.startsWith(ErrorCode.OUT_OF_SEQUENCE) ||
				errorMessage.startsWith(ErrorCode.OUT_OF_ORDER)) {
			
			forcedDisconnect = true;
		}		
		
		if(forcedDisconnect == true) {
			
			try {
				endGame();
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
	}

	/**
	 * Writes outbound messages
	 * 
	 * @param msg
	 * @throws IOException
	 */
	private void writeMessage(String msg) throws IOException {

		if(receiveOnly == false)
			out.write(msg);
	}

	/**
	 * Reads inbound messages
	 * 
	 * @return
	 * @throws IOException
	 */
	private String readMessage() throws IOException {

		// Block forever until a message is received
		return in.read_wait();
	}

	/**
	 * Checks if message is Out of Sequence
	 * 
	 * @param msg
	 * @return boolean
	 */
	private boolean isMessageOutOfSequence(int msgSeq) {

		boolean result = false;

		if ((sequence + 1) % SEQUENCE_LIMIT != msgSeq) {
			
			// Increment sequence number
			sequence = (sequence + 2) % SEQUENCE_LIMIT;
			
			// Send error message
			sendErrorMessage("200-Message Out of Sequence");
			
			result = true;
		}
		else {

			// Increment sequence number
			sequence = (msgSeq + 1) % SEQUENCE_LIMIT;
		}

		return result;
	}

	/**
	 * Check if message is Out of Order
	 * 
	 * @param keyword
	 * @return boolean
	 */
	private boolean isMessageOutOfOrder(String keyword) {

		boolean result = false;

		switch (dfaState) {

		case (DFAState.CONN_INIT):
			if (keyword != InboundMessageKeyword.CRES)
				result = true;
			break;

		case (DFAState.GAME_INIT):
			if (keyword != InboundMessageKeyword.GRES)
				result = true;
			break;

		case (DFAState.LTR_GUESSED):
			if (keyword != InboundMessageKeyword.LRES)
				result = true;
			break;

		case (DFAState.SOL_GUESSED):
			if (keyword != InboundMessageKeyword.SRES)
				result = true;
			break;

		case (DFAState.GAME_END_INIT):
			if (keyword != InboundMessageKeyword.DRES)
				result = true;
			break;

		default:
			result = false;
			break;
		}
		
		// Send error message		
		if(result == true)
			sendErrorMessage("300-Message Out of Order");
			
		return result;
	}
}
