package edu.drexel.cs544.client;

public interface ErrorCode {

	public static final String MALFORMED       = "100";
	
	public static final String OUT_OF_SEQUENCE = "200";
	
	public static final String OUT_OF_ORDER    = "300";
	
}
